\chapter{Ensembles de nombres}

\section{Définitions}

\subsection{Ensembles de nombres}

\begin{definition}
	Un ensemble de nombres est une collection de nombres possédant une ou des propriétés communes. On note $E = \setcond{x}{…}$ l'ensemble des $x$ tel que $…$ .
\end{definition}

\begin{info}[Définition des ensembles : Cantor, Hilbert et Russel]
	La définition des ensembles a été formalisée au XIXè siècle et a posé certains problèmes.
\end{info}

\begin{definition}
	On définit un ensemble des \textbf{entiers naturels}, noté $\N$ contenant les entiers positifs comme $0; 1 ; 2 ; …$.
\end{definition}

\begin{definition}
	L'ensemble contenant les entiers positifs et négatifs est l'ensemble des \textbf{entiers relatifs}.\\
	Il est noté $\Z$.
\end{definition}

\begin{exemple}
  \leavevmode

  \noindent\ligne{1}
\end{exemple}

\begin{definition}
	L'ensemble contenant les nombres $d$ tels qu'il existe $n$ pour lequel
  $d×10^n \in \Z$ est l'ensemble des \textbf{nombres décimaux}.\\
	Il est noté $\Dec$.
\end{definition}

\begin{exemple}
  \leavevmode

  \noindent\ligne{1}
\end{exemple}

\begin{definition}
	L'ensemble contenant les fractions (positives ou négatives) est l'ensemble des \textbf{rationnels}.\\
	Il est noté $\Q$.
\end{definition}

\begin{exemple}
  \leavevmode

  \noindent\ligne{1}
\end{exemple}

\begin{definition}
	L'ensemble contenant tous les nombres est l'ensemble des \textbf{nombres réels}.\\
	Il est noté $\R$.
\end{definition}

\begin{exemple}
  \leavevmode

  \noindent\ligne{1}
\end{exemple}

\begin{remarque}
	On écrit souvent des affirmations comme «Soit $n$ un entier naturel» ou «Soit $x$ un réel positif» pour caractériser les variables utilisées dans une expression.
\end{remarque}

\begin{definition}
	On dit qu'un nombre \textbf{appartient} à un ensemble lorsqu'il en possède les propriétés. On note alors $x \in E$, où $x$ désigne le nombre et $E$ l'ensemble. On dit aussi que $x$ est un élément de $E$.\\
	On dit qu'un ensemble $E$ est \textbf{inclus} dans un ensemble $F$ lorsque tous les éléments de $E$ sont dans $F$. On note alors $E \subset F$.
\end{definition}

\begin{proposition}
	On a les inclusions suivantes : \[ \N \subset \Z \subset \Dec \subset \Q \subset \R \]
\end{proposition}

On peut faire un schéma pour traduire ces inclusions :
\vspace{4cm}

\begin{proposition}
  Le nombre $\frac{1}{3}$ n'est pas décimal.
\end{proposition}
\begin{proof}
  \leavevmode

  \noindent\ligne{9}
\end{proof}

\begin{savoirrediger}[Raisonnement par l'absurde]
  Dans le raisonnement précédent, on a supposé le contraire de ce qu'on
  voulait démontrer pour aboutir à la négation d'une autre hypothèse plus
  fondamentale des mathématiques. On appelle un tel raisonnement «
  raisonnement par l'absurde ».
\end{savoirrediger}

\subsection{Vocabulaire des comparaisons}

\begin{definition}
	Soient $a$ et $b$ deux nombres. On note 
	\begin{itemize}
		\item $a ≤ b$ lorsque $a$ est inférieur ou égal à $b$ ;
		\item $a < b$ lorsque $a$ est strictement inférieur à $b$ ;
		\item $a ≥ b$ lorsque $a$ est supérieur ou égal à $b$ ;
		\item $a > b$ lorsque $a$ est strictement supérieur à $b$ ;
	\end{itemize}
\end{definition}

\begin{exemple}
  \leavevmode

  \noindent\ligne{3}
\end{exemple}

\begin{definition}
	\leavevmode
	\begin{itemize}
		\item Le minimum est le plus petit élément d'une liste de nombre ou d'un ensemble. On le note $\min$.
		\item Le maximum est le plus grand élément d'une liste de nombre ou d'un ensemble. On le note $\max$.
	\end{itemize}
\end{definition}

\begin{exemple}
  \leavevmode

  \noindent\ligne{2}
\end{exemple}

\subsection{Valeur absolue d'un nombre}

\begin{definition}
  On définit la valeur absolue d'un nombre $a$ comme le maximum de $a$ et de
  $-a$. On note $\abs{a} = \max(a ; -a)$.
\end{definition}

\begin{exemple}
  \leavevmode

  \noindent\ligne{1}
\end{exemple}

\begin{proposition}
  Soit $a \in \R$.   Le nombre $\abs{a}$ est positif ou nul.
\end{proposition}
\begin{proof}
  Soit $a \in \R$. Si $a > 0$, alors $-a < 0$ et donc la maximum de $a$ et
  $-a$ est $a > 0$.\\
  Si $a < 0$, alors $-a > 0$ et la maximum de $a$ et $-a$ est $-a > 0$.\\
  Si $a = 0$, alors $\abs{a} = 0$.
\end{proof}

\begin{proposition}\label{chap1:prop:abs_neg}
  Pour tout $a \in \R$, $\abs{a} = \abs{-a}$.
\end{proposition}
\begin{proof}
  Soit $a$ un nombre réel, par exemple positif. Alors $\abs{a} = a$ et
  $\abs{-a} = a$, d'où le résultat.\\
  On peut faire de même dans les cas $a < 0$ et $a = 0$.
\end{proof}

\begin{remarque}
  On dit aussi que $\abs{a}$ est la distance à zéro de $a$.
\end{remarque}

\begin{proposition}
  Pour tout $a$ réel positif, $\abs{a} = a$.
\end{proposition}
\begin{proof}
  \leavevmode

  \noindent\ligne{3}
\end{proof}

\begin{proposition}
  Soient $a$ et $b$ deux nombres réels, le nombre $\abs{a -b} = \abs{b-a}$
  correspond à la distance entre les réels $a$ et $b$.
\end{proposition}
\begin{proof}
  Soient $a$ et $b$ deux nombres réels. En remarquant que $b - a = -(a - b)$
  et en utilisant la proposition \ref{chap1:prop:abs_neg}, on obtient le
  résultat.\\
  Si $a$ et $b$ sont de même signe, alors $a - b$ ou $b - a$ est positif et
  correspond à la distance entre ces deux nombres.\\
  Si $a$ et $b$ sont de signes opposés, alors leur distances est la somme des
  distance à zéro.
\end{proof}

\section{Intervalles de $\R$}

\subsection{Intervalles bornés}

\begin{definition}
	Soient $a$ et $b$ deux nombres réels tel que $a < b$. On appelle intervalle borné l'ensemble des nombres compris entre $a$ et $b$. On distingue :
	\begin{itemize}
		\item $\intv{a}{b} = \setcond{x \in \R}{a ≤ x\ \text{et}\ x ≤ b}$, intervalle fermé ;
		\item $\intv[o]{a}{b} = \setcond{x \in \R}{a < x\ \text{et}\ x < b}$ intervalle ouvert ;
		\item $\intv[l]{a}{b} = \setcond{x \in \R}{a < x\ \text{et}\ x ≤ b}$ intervalle semi-ouvert (ou semi-fermé) ;
		\item $\intv[r]{a}{b} = \setcond{x \in \R}{a ≤ x\ \text{et}\ x < b}$ intervalle semi-fermé (ou semi-ouvert).
	\end{itemize}
\end{definition}
\begin{exemple}
  \leavevmode

  \noindent\ligne{4}
\end{exemple}


\subsection{Intervalles non bornés}

\begin{definition}
	Soit $a$ un nombre réel. On appelle intervalle non borné un des intervalles ouverts :
	\begin{itemize}
		\item $\intv[o]{a}{+\infty} = \setcond{x \in \R}{a < x}$
		\item $\intv[r]{a}{+\infty} = \setcond{x \in \R}{a ≤ x}$ 
		\item $\intv[o]{-\infty}{a} = \setcond{x \in \R}{x < a}$
		\item $\intv[l]{-\infty}{a} = \setcond{x \in \R}{x ≤ a}$ 
	\end{itemize}
\end{definition}
\begin{exemple}
  \leavevmode

  \noindent\ligne{4}
\end{exemple}


\begin{remarque}
	$\R = \intv[o]{-\infty}{+\infty}$
\end{remarque}

\begin{note}[Notations]
	On note quelques intervalles en abrégés :
	\begin{itemize}
		\item $\R_+ = \intv[r]{0}{+\infty}$ les réels positifs ;
		\item $\R_+^* = \intv[o]{0}{+\infty}$ les réels strictement positifs ;
		\item $\R^*$ les réels privés de 0
	\end{itemize}
\end{note}

\begin{savoirfaire}[Passer d'une inégalité à la notation d'intervalle]
	L'inégalité $3.1 ≤ \pi ≤ 3.2 \iff \pi \in \intv[]{3.1}{3.2}$ signifie que
  le nombre $\pi \approx 3.14159…$ est compris entre 3.1 et 3.2.
\end{savoirfaire}

\begin{savoirrediger}[Le symbole $\iff$ (équivalence)]
	La notation «$A \iff B$» où $A$ et $B$ sont des affirmations mathématiques signifie que si $A$ est vraie, alors $B$ est aussi vraie et que si $A$ est fausse, alors $B$ est aussi fausse.\\
	On peut aussi écrire en toutes lettres «$A$ est équivalent à $B$» ou encore «$B$ si et seulement si $A$».
\end{savoirrediger}

\subsection{Intervalles centrés}

\begin{definition}
  Soient $a$ un réel et $r$ un réel strictement positif.\\
  L'intervalle $\intv{a-r}{a+r}$ s'appelle la boule fermée de centre $a$ et
  de rayon $r$.
\end{definition}
\begin{exemple}
  \leavevmode

  \noindent\ligne{3}
\end{exemple}

\begin{proposition}
  Pour tout $a$ réel et pour tout $r$ réel positif non nul, $x \in
  \intv{a-r}{a+r} \iff \abs{x - a} ≤ r$.
\end{proposition}
\begin{proof}
  \begin{align*}
    x \in \intv{a-r}{a+r} & \iff a - r ≤ x ≤ a + r\\
                          & \iff -r ≤ x - a ≤ r
  \end{align*}
  Si $x - a > 0$, alors $x - a ≥ r$ et la condition $x - a ≤ r$ implique que
  $x - a$ est plus petit que $\abs{r}$. On a donc $\abs{x - a} ≤ \abs{r} =
  r$. De même si $x - a < 0$, on a $x - a ≥ -r$ et donc $\abs{x -a} ≤
  \abs{-r} = r$. Ainsi, dans tous les cas, $\abs{x - a} ≤ r$.\\
  La réciproque est assez facile : si $\abs{x - a} ≤ r$, alors le maximum de
  $x - a$ est compris entre $-r$ et $r$ (par définition).
\end{proof}
