\documentclass[french]{beamer}

\input{prez.tex.inc}

\usetheme{Madrid}

\title{Ensembles de nombres}
\subtitle{Intervalles}
\author{Vincent-Xavier \bsc{Jumel}}
\institute{LaSalle Saint-Denis}
\date{5 septembre 2018}

\begin{document}

\begin{frame}
  \maketitle
\end{frame}

\section{Définitions}

\subsection{Ensembles de nombres}

\begin{frame}

\frametitle{Définition}

\begin{definition}
	Un ensemble de nombres est une collection de nombres possédant une ou des propriétés communes. On note $E = \setcond{x}{…}$ l'ensemble des $x$ tel que $…$ .
\end{definition}

\begin{info}[Définition des ensembles : Cantor, Hilbert et Russel]
	La définition des ensembles a été formalisée au XIXè siècle et a posé certains problèmes.
\end{info}

\begin{definition}
	On définit un ensemble des \textbf{entiers naturels}, noté $\N$ contenant les entiers positifs comme $0; 1 ; 2 ; …$.
\end{definition}
\end{frame}

\begin{frame}
	\frametitle{Des ensembles supplémentaires}
	\begin{definition}
		L'ensemble contenant les entiers positifs et négatifs est l'ensemble des \textbf{entiers relatifs}.\\
		Il est noté $\Z$.
	\end{definition}
	
	\begin{definition}
		L'ensemble contenant les fractions (positives ou négatives) est l'ensemble des \textbf{rationnels}.\\
		Il est noté $\Q$.
	\end{definition}
	
	\begin{definition}
		L'ensemble contenant tous les nombres est l'ensemble des \textbf{nombres réels}.\\
		Il est noté $\R$.
	\end{definition}
\end{frame}

\begin{frame}

	\frametitle{Vocabulaire}
	
	\begin{remarque}
		On écrit souvent des affirmations comme «Soit $n$ un entier naturel» ou «Soit $x$ un réel positif» pour caractériser les variables utilisées dans une expression.
	\end{remarque}
	
	\begin{definition}
		On dit qu'un nombre \textbf{appartient} à un ensemble lorsqu'il en possède les propriétés. On note alors $x \in E$, où $x$ désigne le nombre et $E$ l'ensemble. On dit aussi que $x$ est un élément de $E$.\\
		On dit qu'un ensemble $E$ est \textbf{inclus} dans un ensemble $F$ lorsque tous les éléments de $E$ sont dans $F$. On note alors $E \subset F$.
	\end{definition}
	
	\begin{proposition}
		On a les inclusions suivantes : \[ \N \subset \Z \subset \Q \subset \R \]
	\end{proposition}
\end{frame}


\subsection{Vocabulaire des comparaisons}

\begin{frame}
	\frametitle{$<$, $≤$, minimum, …}
	\begin{definition}
		Soient $a$ et $b$ deux nombres. On note 
		\begin{itemize}
			\item $a ≤ b$ lorsque $a$ est inférieur ou égal à $b$ ;
			\item $a < b$ lorsque $a$ est strictement inférieur à $b$ ;
			\item $a ≥ b$ lorsque $a$ est supérieur ou égal à $b$ ;
			\item $a > b$ lorsque $a$ est strictement supérieur à $b$ ;
		\end{itemize}
	\end{definition}
	
	\begin{definition}
		\leavevmode
		\begin{itemize}
			\item Le minimum est le plus petit élément d'une liste de nombre ou d'un ensemble. On le note $\min$.
			\item Le maximum est le plus grand élément d'une liste de nombre ou d'un ensemble. On le note $\max$.
		\end{itemize}
	\end{definition}
\end{frame}


\section{Intervalles de $\R$}

\subsection{Intervalles bornés}

\begin{frame}
	\frametitle{Intervalle bornés}
	\begin{definition}
		Soient $a$ et $b$ deux nombres réels tel que $a < b$. On appelle intervalle borné l'ensemble des nombres compris entre $a$ et $b$. On distingue :
		\begin{itemize}
			\item $\intv{a}{b} = \setcond{x \in \R}{a ≤ x\ \text{et}\ x ≤ b}$, intervalle fermé ;
			\item $\intv[o]{a}{b} = \setcond{x \in \R}{a < x\ \text{et}\ x < b}$ intervalle ouvert ;
			\item $\intv[l]{a}{b} = \setcond{x \in \R}{a < x\ \text{et}\ x ≤ b}$ intervalle semi-ouvert (ou semi-fermé) ;
			\item $\intv[r]{a}{b} = \setcond{x \in \R}{a ≤ x\ \text{et}\ x < b}$ intervalle semi-fermé (ou semi-ouvert).
		\end{itemize}
	\end{definition}
\end{frame}


\subsection{Intervalles non bornés}

\begin{frame}
	\frametitle{Intervalle non bornés}
	\begin{definition}
		Soit $a$ un nombre réel. On appelle intervalle non borné un des intervalles ouverts :
		\begin{itemize}
			\item $\intv[o]{a}{+\infty} = \setcond{x \in \R}{a < x}$
			\item $\intv[r]{a}{+\infty} = \setcond{x \in \R}{a ≤ x}$ 
			\item $\intv[o]{-\infty}{a} = \setcond{x \in \R}{x < a}$
			\item $\intv[l]{-\infty}{a} = \setcond{x \in \R}{x ≤ a}$ 
		\end{itemize}
	\end{definition}
	
	\begin{remarque}
		$\R = \intv[o]{-\infty}{+\infty}$
	\end{remarque}
\end{frame}

\begin{frame}[allowframebreaks]
	\frametitle{Rédaction et raisonnement}
	\begin{block}{Notations}
		On note quelques intervalles en abrégés :
		\begin{itemize}
			\item $\R_+ = \intv[r]{0}{+\infty}$ les réels positifs ;
			\item $\R_+^* = \intv[o]{0}{+\infty}$ les réels strictement positifs ;
			\item $\R^*$ les réels privés de 0
		\end{itemize}
	\end{block}
	
	\begin{savoirfaire}[Passer d'une inégalité à la notation d'intervalle]
		L'inégalité $3.1 ≤ \pi ≤ 3.2 \iff \pi \in \intv[]{3.1}{3.2}$ signifie que le nombre $pi \approx 3.14159…$ est compris entre 3.1 et 3.2.
	\end{savoirfaire}


	\begin{savoirrediger}[Le symbole $\iff$ (équivalence)]
		La notation «$A \iff B$» où $A$ et $B$ sont des affirmations mathématiques signifie que si $A$ est vraie, alors $B$ est aussi vraie et que si $A$ est fausse, alors $B$ est aussi fausse.\\
		On peut aussi écrire en toutes lettres «$A$ est équivalent à $B$» ou encore «$B$ si et seulement si $A$».
	\end{savoirrediger}

\end{frame}

\end{document}
