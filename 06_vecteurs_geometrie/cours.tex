\chapter{Vecteurs --- Aspects géométriques}

\section{Généralités}

\subsection{Définition}

\begin{definition}
	Soient $A$ et $B$ deux points du plan.\\
	À la translation qui envoie $A$ sur $B$, on associe un «objet mathématique», le \textbf{vecteur} $\vv{AB}$.\\
	Celle-ci associe à tout point $C$ du plan un unique point $D$ tel que $[AD]$ et $[BC]$ ont le même milieu.
\end{definition}

\begin{note}[Tracé des points avec le même milieu]
	\begin{multicols}{2}
		\begin{center}
			\begin{tikzpicture}[>=latex]
				\tkzDefPoint(0,0){A}
				\tkzDefPoint(4,3){B}
				\tkzDrawPoints(A,B)
				\tkzLabelPoints(A,B)
				\tkzDrawSegment[red,->](A,B)
				\tkzDefPoint(-1,2){C}
				\tkzDrawPoints(C)
				\tkzLabelPoints(C)
				\tkzDrawSegment[thick,dashed](B,C)
				\tkzDefMidPoint(B,C)
				\tkzGetPoint{I}
				\tkzDrawPoints(I)
				\tkzLabelPoints(I)
				\tkzMarkSegments[mark=||](C,I I,B)
				\tkzDefPointBy[symmetry=center I](A){}
				\tkzGetPoint{D}
				\tkzDrawPoints(D)
				\tkzLabelPoints(D)
				\tkzDrawSegment[dashed](A,D)
				\tkzMarkSegments[mark=o](A,I I,D)
				\tkzDrawSegment[thick,blue,->](C,D)
			\end{tikzpicture}
		\end{center}
		\begin{center}
			\begin{tikzpicture}[>=latex,scale=0.7]
				\tkzDefPoint(0,0){A}
				\tkzDefPoint(4,3){B}
				\tkzDrawPoints(A,B)
				\tkzLabelPoints(A,B)
				\tkzDefPoint(6,4.5){C}
				\tkzDrawPoints(C)
				\tkzLabelPoints(C)
				\tkzDrawSegment[dashed](B,C)
				\tkzDefMidPoint(B,C)
				\tkzGetPoint{I}
				\tkzDrawPoints(I)
				\tkzLabelPoints(I)
				\tkzMarkSegments[mark=||](C,I I,B)
				\tkzDefPointBy[symmetry=center I](A){}
				\tkzGetPoint{D}
				\tkzDrawPoints(D)
				\tkzLabelPoints(D)
				\tkzDrawSegment[dashed](A,D)
				\tkzMarkSegments[mark=o](A,I I,D)
				\tkzDrawSegment[thick,red,->](A,B)
				\tkzDrawSegment[thick,blue,->](C,D)
			\end{tikzpicture}
		\end{center}
	\end{multicols}
\end{note}

\begin{definition}
	Soit $\vv{AB}$ un vecteur.
	\begin{itemize}
		\item $A$ s'appelle l'origine du vecteur.
		\item $B$ s'appelle l'extrémité (ou destination) du vecteur.
		\item La droite $(AB)$ est la direction du vecteur.
		\item La distance $AB$ s'appelle la norme du vecteur. Elle est notée $\norm{\vv{AB}}$.
		\item On dit que le vecteur est orienté de $A$ vers $B$.
	\end{itemize}
\end{definition}

\subsection{Égalité de vecteurs}

\begin{definition}
	Deux vecteurs sont égaux si et seulement s'ils définissent la même translation.
\end{definition}

\begin{proposition}\label{vecteurs:prop:egalite_ssi_parallelogramme}
	Soient $\vv{AB}$ et $\vv{CD}$ deux vecteurs.\\
	$\vv{AB} = \vv{CD}$ si et seulement si $ABDC$ est un parallélogramme, éventuellement aplati.
\end{proposition}
\begin{proof}
	Si $\vv{AB} = \vv{CD}$, alors $[AD]$ et $[BC]$ ont le même milieu et donc $ABDC$ est un parallélogramme.\\
	Réciproquement, si $ABDC$ est un parallélogramme, alors $[AD]$ et $[BC]$ ont le même milieu et donc $\vv{AB} = \vv{CD}$.
\end{proof}

\begin{remarque}
	Attention à l'ordre des lettres dans le parallélogramme.
\end{remarque}

\begin{proposition}\label{vecteurs:prop:egalite_ssi_sens_norme_direction}
	Soient $\vv{AB}$ et $\vv{CD}$ deux vecteurs.\\
	$\vv{AB} = \vv{CD}$ si et seulement s'ils ont
	\begin{itemize}
		\item même direction,
		\item même sens,
		\item même norme.
	\end{itemize}
\end{proposition}
\begin{proof}
	Si $\vv{AB} = \vv{CD}$, alors d'après la proposition~\ref{vecteurs:prop:egalite_ssi_parallelogramme}, $ABDC$ est donc un parallélogramme.\\
	On a donc $AB = CD$ et donc l'égalité des normes.\\
	On a également $(AB) \parallel (CD)$ et donc $\vv{AB}$ et $\vv{CD}$ ont la même direction.\\
	Enfin, comme le parallélogramme s'appelle $ABDC$, on a bien $A → B$ et $C → D$ qui sont dans le même sens.\\[2ex]
	Réciproquement, si les trois conditions sont vérifiées, $ABDC$ est un parallélogramme, d'où le résultat.
\end{proof}

\begin{savoirrediger}[Démontrer l'égalité de deux vecteurs]
	Pour démontrer l'égalité de deux vecteurs, on se sert
	\begin{itemize}
		\item soit de la définition,
		\item soit de la proposition~\ref{vecteurs:prop:egalite_ssi_parallelogramme},
		\item soit de la proposition~\ref{vecteurs:prop:egalite_ssi_sens_norme_direction}.
	\end{itemize}
\end{savoirrediger}

\begin{definition}
	\leavevmode
	\begin{itemize}
		\item Le vecteur $\vv{AA}$ existe et s'appelle le vecteur nul. Il est noté $\vv{0}$.
		\item Le vecteur opposé au vecteur $\vv{AB}$ est le vecteur $\vv{BA}$.
	\end{itemize}
\end{definition}

\section{Opérations sur les vecteurs}

\subsection{Somme et différence de vecteurs}

\begin{definition}
	Soient $\vv{AB}$ et $\vv{BC}$ deux vecteurs.\\
	La somme de deux vecteurs $\vv{AB}$ et $\vv{BC}$ correspond à la translation qui envoie $A$ sur $B$, puis $B$ sur $C$.
\end{definition}

\begin{proposition}
	La somme de deux vecteurs est un vecteur.
\end{proposition}
\begin{proof}
	Soient $\vv{AB}$ et $\vv{CD}$ deux vecteurs. Considérons le vecteur $\vv{BE}$ tel que $\vv{BE} = \vv{CD}$ -- celui-ci existe, il suffit de construire le parallélogramme.\\
	On peut considérer désormais la transformation qui envoie $A$ sur $B$ et $B$ sur $E$. Cette transformation est équivalente à la transformation qui envoie $A$ sur $E$, à laquelle on peut associer le vecteur $\vv{AE}$.
\end{proof}

\begin{savoirfaire}[Construire la somme de deux vecteurs]
	\begin{center}
		\begin{tikzpicture}[>=latex]
		\tkzDefPoint(0,0){A}
		\tkzDefPoint(4,3){B}
		\tkzDrawPoints(A,B)
		\tkzLabelPoints(A,B)
		\tkzDrawSegment[thick,red,->](A,B)
		\tkzDefPoint(-1,2){C}
		\tkzDefPoint(-3,4){D}
		\tkzDrawPoints(C,D)
		\tkzLabelPoints(C)
		\tkzLabelPoints[above](D)
		\tkzDrawSegment[thick,blue,->](C,D)
		\tkzDrawSegment[dashed](C,B)
		\tkzDefPointBy[translation= from C to B](D){}
		\tkzGetPoint{E}
		\tkzDrawPoints(E)
		\tkzLabelPoints[above](E)
		\tkzDrawSegment[dashed](D,E)
		\tkzDrawSegment[blue,dashed,->](B,E)
		\tkzDrawSegment[thick,green!75!black,->](A,E)
		\tkzDefPointBy[translation= from C to A](D){}
		\tkzGetPoint{F}
		\tkzLabelPoints[above left](F)
		\tkzDrawSegment[blue,dashed,->](A,F)
		\tkzDrawSegment[red,dashed,->](F,E)
		\end{tikzpicture}
	\end{center}
\end{savoirfaire}

\begin{remarque}
	La somme de deux vecteurs correspond à une des diagonales du parallélogramme.
\end{remarque}

\begin{info}[Chasles]
	Michel Chasles est un mathématicien français qui a travaillé sur les vecteurs.
\end{info}

\begin{proposition}
	Soit $\vv{AB}$ un vecteur.\\
	Alors $\vv{AB} + \vv{BA} = \vv{0}$.
\end{proposition}
\begin{proof}
	Il suffit de faire la construction.
\end{proof}

\begin{corollaire}
	Soit $\vv{AB}$ un vecteur.\\
	$\vv{BA} = -\vv{AB}$
\end{corollaire}
\begin{proof}
	On applique la règle de manipulation algébrique $a + b = 0 \iff a + b + (-b) = 0 + (-b) \iff a = -b$.
\end{proof}

\begin{savoirfaire}[Construire la différence de deux vecteurs]
	On procède comme pour la somme, mais en faisant une somme algébrique. Ainsi, $\vv{AB} - \vv{GF} = \vv{AB} + (-\vv{GF}) = \vv{AB} + \vv{FG}$.
\end{savoirfaire}

\begin{proposition}[Relation de Chasles]
	Soient $\vv{AB}$ un vecteur et $C$ un point.\\
	Alors $\vv{AB} = \vv{AC} + \vv{CB}$.
\end{proposition}
\begin{proof}
	C'est une conséquence de l'addition des vecteurs.
\end{proof}

\subsection{Produit d'un vecteur par un scalaire}

\begin{definition}
	Soient $\vv{AB}$ un vecteur et $k$ un nombre réel.\\
	Le produit $k × \vv{AB}$ est un vecteur, noté $k\vv{AB}$.
\end{definition}

\begin{remarque}\label{vecteurs:rem:produit}
	Le vecteur $k\vv{AB}$ possède la même direction que le vecteur $\vv{AB}$. Cependant, la norme de $\norm{k\vv{AB}}$ est $\abs{k}×\norm{\vv{AB}}$, où $\abs{k}$ désigne la valeur absolue de $k$ (ou distance à zéro). Le sens $k\vv{AB}$ est le même que $\vv{AB}$ si $k$ est positif et de sens opposé si $k<0$. Enfin, si $k = 0,\ 0×\vv{AB} = \vv{0}$.
\end{remarque}

\begin{savoirrediger}[Savoir démontrer une égalité vectorielle en utilisant la relation de Chasles]\label{vecteurs:savoirrediger:gravite}
	Soient $A,B$ et $C$ trois points non alignés. On définit $G$ tel que $\vv{GA}  + \vv{GB} + \vv{GC} = \vv{0}$.\\
	Montrer que pour tout point $M$ du plan, $\vv{MA} + \vv{MB} + \vv{MC} = 3\vv{MG}$.\\[2ex]
	D'après la relation de Chasles, $\vv{GA} = \vv{GM} + \vv{MA}$. De même, $\vv{GB} = \vv{GM} + \vv{MB}$ et $\vv{GC} = \vv{GM} + \vv{MC}$.\\
	On a donc $\vv{GA}  + \vv{GB} + \vv{GC} = \vv{GM} + \vv{MA} + \vv{GM} + \vv{MB} + \vv{GM} + \vv{MC}$. Cette dernière égalité donne, en regroupant les termes \\
	$\vv{GM} + \vv{GM} + \vv{GM} + \vv{MA} + \vv{MB} + \vv{MC} = \vv{0}$. D'où \\
	$3\vv{GM} + \vv{MA} + \vv{MB} + \vv{MC} = \vv{0}$ et enfin,\\
	$\vv{MA} + \vv{MB} + \vv{MC} = 3\vv{MG}$.
\end{savoirrediger}

\section{Applications}

\subsection{Parallélisme et alignement}

\begin{definition}
  \label{06_vecteurs_geometrie:def:colineaire}
	Soient $\vv{AB}$ et $\vv{CD}$ deux vecteurs.\\
	S'il existe $k≠0$ tel que $\vv{AB} = k \vv{CD}$, alors on dit que les vecteurs sont \textbf{colinéaires}.
\end{definition}

La «réciproque» de cette définition coule de source.

\begin{proposition}
	Soient $\vv{AB}$ et $\vv{CD}$ deux vecteurs colinéaires.\\
	Alors les droites $(AB)$ et $(CD)$ sont parallèles ou confondues.\\
	Si de plus, $A,B$ et $C$ ou $A,B$ et $D$ sont alignés alors, $A,B,C$ et $D$ sont alignés.
\end{proposition}
\begin{proof}
	S'il existe $k≠0$ tel que $\vv{AB} = k\vv{CD}$, alors d'après la remarque~\ref{vecteurs:rem:produit}, les vecteurs $\vv{AB}$ et $\vv{CD}$ ont même direction.\\
	Si, par exemple, $C \in (AB)$, alors les droites $(CD)$ et $(AB)$ sont confondues.
\end{proof}

\begin{savoirrediger}[Démontrer parallélisme ou alignement avec les vecteurs]
	Pour démontrer que des points sont alignés, on essaie de trouver une relation vectorielle de la forme $\vv{AB} = k\vv{AC}$.
\end{savoirrediger}

\subsection{Milieu d'un segment}

\begin{proposition}
	Soient $A$ et $B$ deux points distincts.\\
	Le milieu $I$ du segment $[AB]$ est le point défini par $\vv{AI} = \frac12\vv{AB}$.
\end{proposition}
\begin{proof}
	$I \in [AB]$ et comme $AI = \frac12 AB$, alors $\norm{\vv{AI}} = \frac12 \norm{\vv{AB}}$. Enfin, comme $\vv{AI}$ est de même sens que $\vv{AB}$, le point $I$ est située dans la direction de $B$, donc à l'intérieur du segment $[AB]$.
\end{proof}

\begin{corollaire}
	Soit $AB$ un segment. \\
	Alors $I$ est le milieu du segment, si et seulement si $\vv{AI} = \vv{IB}$.
\end{corollaire}
\begin{proof}
	Si $I$ est le milieu du segment $[AB]$, alors $\vv{AI} = \frac12\vv{AB}$, mais aussi $\vv{BI} = \frac12\vv{BA}$, d'où $\vv{IB} = \frac12\vv{AB}$. On a bien alors $\vv{AI} = \frac12\vv{AB} = \vv{IB}$.
\end{proof}

\begin{note}[Le centre de gravité d'un triangle]
	Soient $A,B$ et $C$ trois points non alignés. On définit $G$ tel que $\vv{GA}  + \vv{GB} + \vv{GC} = \vv{0}$.\\
	On a vu, au paragraphe~\ref{vecteurs:savoirrediger:gravite} que, pour tout point $M$ du plan, $\vv{MA} + \vv{MB} + \vv{MC} = 3\vv{MG}$.\\
	Prenons, $M = I$, milieu de $[AB]$. L'égalité précédente devient $\vv{IA} + \vv{IB} + \vv{IC} = 3\vv{IG}$.\\
	Or $\vv{IA} + \vv{IB} = -\vv{AI} + \vv{IB} = -\vv{IB} + \vv{IB} = \vv{0}$.\\
	On obtient donc $\vv{IC} = 3 \vv{IG}$ ou encore $\vv{IG} = \frac13 \vv{IC}$.\\
	Le point $G$ appartient à la médiane issue de $C$ passant par $I$, milieu de $[AB]$. De la même façon, on montre qu'il appartient aux autres médianes. Le point $G$ ainsi défini est donc l'intersection des médianes du triangle.\\
	On l'appelle centre de gravité à cause de sa définition : $\vv{GA}  + \vv{GB} + \vv{GC} = \vv{0}$. Celle-ci signifie que si les masses sont les mêmes en $A,\ B$ et $C$, alors le point $G$ est le point d'équilibre du triangle.
\end{note}

\section{Décomposition dans une base}

\subsection{Notion de base de vecteur dans le plan}

\begin{proposition}
  Soient $\vv{u}$ et $\vv{v}$ deux vecteurs non colinéaires du plan.\\
  Si $\vv{w}$ est un vecteur du plan, alors il existe $a$ et $b$ deux réels
  tels que $\vv{w} = a\vv{u} + b\vv{b}$
\end{proposition}
\begin{proof}
  Soient $\vv{u}$ et $\vv{v}$ deux vecteurs non colinéaires du plan.\\
  Soit $\vv{w}$ un vectur du plan
  \begin{itemize}
    \item si $\vv{w} = \vv{0}$, alors $a = 0$ et $b = 0$ conviennent ;
    \item si $\vv{w}$ est colinéaire à $\vv{u}$ (respectivement $\vv{v}$),
      alors d'après \ref{06_vecteurs_geometrie:def:colineaire}, on a $a ≠ 0$
      (resp. $b ≠ 0$) ;
    \item sinon, prenont $\vv{AB}$ un représentant de $\vv{w}$ et
      construisons $M$ tel que $\vv{AM}$ soit colinéaire à $\vv{u}$ et
      $\vv{BM}$ soit colinéaire à $\vv{v}$, de telle sorte que $\vv{AB} =
      a\vv{u} + b\vv{b}$
  \end{itemize}
\end{proof}

\begin{definition}[base vectorielle]
  Un couple de vecteurs non colinéaire du plan s'appelle une base
  vectorielle du plan.
\end{definition}

\begin{definition}[coordonnées dans une base]
  Soit $(\vv{u}, \vv{v})$ une base du plan et $\vv{w}$ un vecteur tel que
  $\vv{w} = a \vv{u} + b\vv{v}$. On dit que $\begin{pmatrix} a \\b
  \end{pmatrix}_{(\vv{u},\vv{v})}$ sont les coordonnées du vecteur $\vv{w}$
  dans la base $(\vv{u}, \vv{v})$.
\end{definition}

\begin{definition}[base orthogonale]
  Soit $(\vv{u}, \vv{v})$ une base du plan. Si les directions des deux
vecteurs sont perpendiculaires, on dit que la base est \emph{orthogonale}.
\end{definition}

\subsection{Projeté orthogonal}
