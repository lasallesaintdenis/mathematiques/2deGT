\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}
\newtheorem{theoreme}{Théorème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}

\title{Vecteurs}
\author{2\up{nde}}
\date{}

\begin{document}

\textcolor{violet}{IV.b \quad Colinéarité}

\begin{definition}[Colinéarité]
  On dit que deux vecteurs $\vec{u}$ et $\vec{v}$ sont colinéaires si
  et seulement si il existe $k\in\mathbf{R^*}$ tel que $\vec{v} =
  k\vec{u}$ ou $\vec{u} = k\vec{v}$.
\end{definition}

\begin{propriete}[une autre formulation de la colinéarité]
Deux vecteurs non nuls $\Vecteur{AB}$ et $\Vecteur{CD}$ sont \emph{colinéaires}
\begin{itemize}
\item[$\Leftrightarrow$] les droites $(AB)$ et $(CD)$ sont parallèles. 
\item[$\Leftrightarrow$] les coordonnées de $\Vecteur{AB}$ et $\Vecteur{CD}$ sont proportionnelles
\item[$\Leftrightarrow$]  $x_{\Vecteur{AB}} \times y_{\Vecteur{CD}}=y_{\Vecteur{AB}} \times x_{\Vecteur{CD}}$
\item[$\Leftrightarrow$] il existe $k\in\R$ tel que $\Vecteur{AB}=k\,\Vecteur{CD}$ (ou $k\,\Vecteur{AB}=\Vecteur{CD}$)
\end{itemize}
\end{propriete}

\begin{exemple}
 Soit $ABC$ un triangle rectangle en $A$.
    \begin{itemize}
    	\item $\Vecteur{AD}=\Vecteur{BA}$
        		\item $\Vecteur{CE}=\Vecteur{CB}+\Vecteur{CD}$
    \end{itemize}
 Quelle est la nature du quadrilatère $BCDE$ ? Justifier.\\
 \ligne{8}
\end{exemple}

\begin{exemple}
Soient $A(0;1)$ ,$B(\sqrt 2;2)$ et $C(2;\sqrt 2+1)$  . Montrer que les points sont alignés?
\begin{enumerate}
\item Calculer les coordonnées du vecteur $\Vecteur{AB}$\\
\ligne{4}
\item Calculer les coordonnées du vecteur $\Vecteur{AC}$\\
\ligne{4}
\item Montrer que les vecteurs $\Vecteur{AB}$ et $\Vecteur{AC}$ sont colinéaires.\\
\ligne{3}
\item En déduire que les points $A,B$ et $C$ sont alignés.\\
\ligne{4}
\end{enumerate}
\end{exemple}

\begin{propriete}[colinéarité en terme de droites]
  Deux vecteurs sont colinéaires
  \begin{itemize}
\item[$\Leftrightarrow$] les vecteurs ont même direction
\item[$\Leftrightarrow$] les vecteurs sont portés par des droites parallèles.
\end{itemize}
\end{propriete}

\begin{propriete}
$I$ est le milieu du segment$[AB]$
\begin{itemize}
\item[$\Leftrightarrow$] $\Vecteur{AI}=\Vecteur{IB}$
\item[$\Leftrightarrow$] $\Vecteur{AB}=2\Vecteur{AI}$\\

\item[$\Leftrightarrow$] $\Vecteur{AI}=\dfrac{1}{2}\Vecteur{AB}$
\end{itemize}
\end{propriete}

\begin{exemple}
Soit le triangle $ABC$, $I$ est le milieu du segment $[AB]$ et J est le milieu du segment $[AC]$.\\
 Montrer que $\Vecteur{IJ}=\dfrac{1}{2}\Vecteur{BC}$.\\
 \ligne{13}
\end{exemple}
\end{document}
