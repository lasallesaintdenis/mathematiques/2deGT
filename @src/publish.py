#!/usr/bin/env python3
import csv
import sys
import subprocess
import requests
import os
from datetime import datetime


def publish(username, password, document):
    document = document.split('/')[-1]
    r = requests.put(
        f'https://educloud.ac-creteil.fr/remote.php/dav/files/vjumel/2deGT/{document}',
        auth=(username, password),
        files={'file': open(document, 'rb')}
    )
    return r.status_code


def read_and_publish(username, password):
    with open('compile.csv', 'r') as fp:
        fichiers = csv.DictReader(fp, delimiter=';')
        for seance in fichiers:
            documents = seance['fichiers'].split(' ')
            status = []
            for document in documents:
                if seance['dates'] == datetime.strftime(datetime.now(),
                                                        '%Y-%m-%d'):
                    status.append(
                        publish(
                            username,
                            password,
                            document.replace('.tex', '.pdf')
                        )
                    )
                    share(username, password, document)
    return tuple(status)


def share(username, password, document):
    r = requests.post(
        'https://educloud.ac-creteil.fr/ocs/v2.php/apps/files_sharing/api/v1/shares',
        headers={'OCS-APIRequest': 'true'},
        auth=(username, password),
        data={'path': f'2deGT/{document}',
              'shareType': 4,
              'shareWith': os.getenv('GITLAB_USER_EMAIL')
              }
    )
    return r.status


if __name__ == '__main__':
    username, password = tuple(sys.argv[1:])
    read_and_publish(username, password)
