\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbf{R}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}



% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{teal}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}

\title{Fonction affine}
\author{ 2\up{nd}}
\date{}

\begin{document}


\maketitle\\

\section{fonction affine}
\subsection{définition}
Soit $a$ et $b$ deux réels.\\
La fonction $f$ définie sur $\R$ par $f(x)=ax+b$ est une fonction affine.\\
\begin{exemple}
 \begin{itemize}
 \item La fonction $f$ définie sur $\R$  par $f(x) = \dfrac{x}{2}-3$ est une fonction affine avec $a=\dfrac{1}{2}$ et $b=-3$.
\item La fonction $f$ définie pour tout réel $x \ne 0$ par $f(x) = \dfrac{2}{x}-3$ n'est pas une fonction affine.
 \end{itemize}
\end{exemple}
\subsubsection{cas particuliers}
\begin{itemize}
 \item Dans le cas où $b=0$, la fonction $f$ définie sur $\R$ par $f(x)=ax$ est appelée fonction linéaire.
 \item Dans le cas où $a=0$, la fonction $f$ définie sur $\R$ par $f(x)=b$ est une fonction constante.
 \end{itemize}

\subsection{proportionnalité des accroissements}
$f$ est une fonction affine si, et seulement si, pour tous nombres réels distincts, on a : 
\[\dfrac{f\left( x_2 \right) - f\left( x_1 \right)}{x_2 - x_1} = a\]

\begin{demonstration}
 \begin{itemize}
 \item Soit $f$ une fonction affine définie sur $\R$ par $f(x)=ax+b$.\\
 Alors pour tous réels $x_1 \ne x_2$ on a : \[\dfrac{f\left( x_2 \right) - f\left( x_1 \right)}{x_2 - x_1} = \dfrac{a x_2 +b-ax_1 -b}{x_2 - x_1} = \dfrac{a \left(x_2 -x_1\right)}{x_2 - x_1} =a\]
 \item Soit $f$ une fonction définie sur $\R$ telle que, pour tous réels $x_1 \ne x_2$, on a \: $\dfrac{f\left( x_2 \right) - f\left( x_1 \right)}{x_2 - x_1} = a$.
 
 Alors, en particulier pour tout réel $x \ne 0$ on a : \[\dfrac{f(x) - f(0)}{x-0} = \dfrac{f(x)-f(0)}{x} = a\]
 D'où $f(x)-f(0)=ax$. Soit en notant l'image de 0 $f(0)=b$, on obtient pour tout réel $x$, $f(x)=ax+b$.
 
 Ainsi, $f$ est une fonction affine.
 \end{itemize}
\end{demonstration}
\begin{exemple}
  Déterminer la fonction affine $f$ telle que $f(-6)=5$ et $f(3)=-1$.
	
$f$ est une fonction affine d'où pour tout réel $x$, $f(x)=ax+b$ avec \[a=\dfrac{f(3) - f(-6)}{3 - (-6)} \qquad \text{Soit} \qquad a=\dfrac{-1-5}{3 +6}=-\dfrac{2}{3} \]
Ainsi, $f(x)=-\dfrac{2}{3}x+b$. Or $f(3)=-1$ d'où \[\begin{split} -\dfrac{2}{3} \times 3 +b =-1 & \Leftrightarrow -2 +b =-1\\
&  \Leftrightarrow b =1
\end{split}\]

$f$ est la fonction définie sur $\R$ par $f(x)=-\dfrac{2}{3}x+1$.
\end{exemple}
\subsection{variation}
Soit $a$ et $b$ deux réels.
\begin{itemize}
 \item Si $a$ est positif, la fonction affine $f$ définie sur $\R$  par $f(x) = ax+b$ est croissante.
\item Si $a$ est négatif, la fonction affine $f$ définie sur $\R$  par $f(x) = ax+b$ est décroissante.
 \end{itemize}

\begin{demonstration}
 	\begin{multicols}{2}
 	Si $a$ est positif :
 
 Soit $x_1$ et $x_2$ deux réels tels que \; $x_1 \leqslant x_2 $
 
 Comme $a \geqslant 0$, \; $ax_1\leqslant ax_2$.\\ 
 D'où \; $ax_1 +b \leqslant ax_2+b$
 
 Soit $f\left( x_1 \right) \leqslant f\left( x_2 \right)$

 Donc $f$ est croissante

Si $a$ est négatif :
 
 Soit $x_1$ et $x_2$ deux réels tels que  $x_1 \leqslant x_2 $
 
 Comme $a \leqslant 0$, $ax_1\geqslant ax_2$. \\
 D'où $ax_1 +b \geqslant ax_2+b$
 
 Soit $f\left( x_1 \right) \geqslant f\left( x_2 \right)$

 Donc $f$ est décroissante
 \end{multicols}
 	\end{demonstration}
 	
 \subsection{signe de $ax+b$ avec $a \ne 0$}
 Soit $f$ la fonction affine définie sur $\R$ par $f(x)= ax+b$ avec $a \ne 0$.

 $f(x)$ est du signe de $a$ pour les valeurs de $x$ supérieures à  $-\dfrac{b}{a}$.
  
\begin{demonstration}
 Si $a\ne0$ alors l'équation $ax+b=0$ admet pour solution $x=-\dfrac{b}{a}$. 
\begin{multicols}{2}
Si $a >0$ alors  $f$ est strictement croissante : 
 
donc pour tout réel $x < -\dfrac{b}{a}$,  $f(x) < f\left(-\dfrac{b}{a}\right)$
 
 soit pour tout réel $x < -\dfrac{b}{a}$,  $f(x) < 0$
 
D'où le tableau du signe de $f(x)$

\begin{tikzpicture}
     \tkzTabInit[espcl=2.5]
      {$x$ / 1   , $f(x)$ / 1}%
      {$\-\infty$, $-\dfrac{b}{a}$,$+\infty$  }%
      \tkzTabLine{,- ,z,+}
    \end{tikzpicture}\\
    
    Si $a <0$ alors  $f$ est strictement décroissante : 
 
donc pour tout réel $x < -\dfrac{b}{a}$,  $f(x) > f\left(-\dfrac{b}{a}\right)$
 
 soit pour tout réel $x < -\dfrac{b}{a}$,  $f(x) > 0$
 
D'où le tableau du signe de $f(x)$

\begin{tikzpicture}
     \tkzTabInit[espcl=2.5]
      {$x$ / 1   , $f(x)$ / 1}%
      {$\-\infty$, $-\dfrac{b}{a}$,$+\infty$  }%
      \tkzTabLine{,+ ,z,-}
    \end{tikzpicture}\\
    \end{multicols}
 \end{demonstration}   
    \subsection{courbe représentative}
Soit $a$ et $b$ deux réels.

La courbe représentative de la fonction affine $f$ définie sur $\R$ par $f(x)=ax+b$ est la droite $\mathcal{D}$ d'équation $y=ax+b$.

\begin{multicols}{3}
$a<0$\\
\begin{tikzpicture}[scale=0.6]
\draw [gray](-4,-3) grid (4,6);
\draw[->, very thick] (-4,0) -- (4,0) node[below]{$x$};
\draw[->, very thick] (0,-3) -- (0,6) node[left]{$y$};
\draw (0,0) node[below left]{$0$} (0,1) node [left]{$1$} (1,0) node [below]{$1$} (3,0) node[violet,below]{$\dfrac{-b}{a}$};
\draw[blue, very thick] plot [domain=-4:4]  (\x,{-0.5*(\x)+1.5});
\end{tikzpicture}

$a=0$\\
\begin{tikzpicture}[scale=0.6]
\draw [gray](-4,-3) grid (4,6);
\draw[->, very thick] (-4,0) -- (4,0) node[below]{$x$};
\draw[->, very thick] (0,-3) -- (0,6) node[left]{$y$};
\draw (0,0) node[below left]{$0$} (0,1) node [left]{$1$} (1,0) node [below]{$1$};
\draw[blue, very thick] (-4,2.5)--(4,2.5);
\end{tikzpicture}

$a>0$\\
\begin{tikzpicture}[scale=0.6]
\draw [gray](-4,-3) grid (4,6);
\draw[->, very thick] (-4,0) -- (4,0) node[below]{$x$};
\draw[->, very thick] (0,-3) -- (0,6) node[left]{$y$};
\draw (0,0) node[below left]{$0$} (0,1) node [left]{$1$} (1,0) node [below]{$1$} (-2,0) node[violet,below]{$\dfrac{-b}{a}$};
\draw[blue, very thick] plot [domain=-4:4]  (\x,{0.5*(\x)+1});
\end{tikzpicture}
\end{multicols}

\section{inéquations}

Pour résoudre une inéquation à une inconnue on peut être amené à  étudier le signe d'une expression.

\begin{center}
Résoudre $A(x) \leqslant B(x)$ équivaut à  résoudre $A(x) - B(x) \leqslant 0$.
\end{center}

\subsection{\'Etude du signe d'un produit}

\subsubsection{règle des signes d'un produit}
 Le produit de deux nombres de même signe est positif.  Le produit de deux nombres de signes contraires est négatif. 
 
 \subsubsection{tableau de signes d'un produit}
Pour étudier le signe d'un produit :

\begin{itemize}
 \item On étudie le signe de chaque facteur. 
 \item On regroupe dans un tableau le signe de chaque facteur. La première ligne du tableau contenant les valeurs, rangées dans l'ordre croissant, qui annulent chacun des facteurs.
 \item On utilise la règle des signes pour remplir la dernière ligne.
 \end{itemize} 

\textsf{\textsc{\small{exemple}}}
Résoudre dans $\R$  l'inéquation $\left( 2x+3 \right)^2 \leqslant \left( 3x-1 \right)^2$

Pour tout réel $x$, \[
\begin{split}
\left( 2x+3 \right)^2 \leqslant \left( 3x-1 \right)^2 & \Leftrightarrow \left( 2x+3 \right)^2 - \left( 3x-1 \right)^2 \leqslant 0 \\
&  \Leftrightarrow \left[ (2x+3) +( 3x-1) \right]\times\left[ (2x+3) -( 3x-1) \right] \leqslant 0 \\
&  \Leftrightarrow \left(2x+3 + 3x-1 \right)\left(2x+3 - 3x+1 \right) \leqslant 0\\
&  \Leftrightarrow \left(5x+2 \right)\left(4-x \right) \leqslant 0\\
\end{split}
\]
\'Etudions le signe du produit $\left(5x+2 \right)\left(4-x \right)$ à  l'aide d'un tableau de signe.

On étudie les signe de chacun des facteurs du produit :
\[ 5x+2 \leqslant 0 \Leftrightarrow x \leqslant - \dfrac{2}{5} \qquad \text{et} \qquad 4-x \leqslant 0 \Leftrightarrow x \geqslant 4\]
On résume dans un seul tableau le signe de chacun des facteurs et, on en déduit le signe du produit en utilisant la règle des signes d'un produit  :\\
\begin{tikzpicture}
     \tkzTabInit[lgt=3,espcl=3.5]
      {$x$ / 1   , $5x+2$ / 1, $4-x$/1, $(5x+2)(4-x)$/1}%
      {$\-\infty$, $-\dfrac{2}{5}$,$4$,$+\infty$  }%
      \tkzTabLine{,- ,z,+,t,+}
      \tkzTabLine{,+,t,+,z}
      \tkzTabLine{,-,z,+,z,-}
    \end{tikzpicture}\\
    
    L'ensemble des solutions de l'inéquation $\left(5x+2 \right)\left(4-x \right) \leqslant 0$ est $S=\left] - \infty ; -\dfrac{2}{5}\right] \cup \left[\vphantom{\dfrac{2}{5}} 4 ; + \infty \right[$.

\subsection{\'Etude du signe d'un quotient}
\subsubsection{Règle des signes d'un quotient}
 Le quotient de deux nombres de même signe est positif.  Le quotient de deux nombres de signes contraires est négatif. 
  
 \subsubsection{tableau de signes d'un quotient}
Pour étudier le signe d'un quotient :
\begin{itemize}
 \item On cherche les valeurs qui annulent le dénominateur (valeurs interdites). 
 \item On regroupe dans un tableau le signe de chaque terme. 
 \item On utilise la règle des signes pour remplir la dernière ligne.
 \end{itemize} 

\begin{exemple}
Résoudre dans $\R$  l'inéquation $\dfrac{2x+7}{3x+2} \geqslant 2$

Le quotient $\dfrac{2x+7}{3x+2}$ est défini pour tout réel $x$ tel que le dénominateur $3x+2 \ne 0$.

Comme $3x+2 \ne 0 \Leftrightarrow x \ne -\dfrac{2}{3}$, le quotient $\dfrac{2x+7}{3x+2}$ est défini pour tout réel $x \ne -\dfrac{2}{3}$ : \[
\begin{split}
\dfrac{2x+7}{3x+2} \geqslant 2 & \Leftrightarrow \dfrac{2x+7}{3x+2} -2 \geqslant 0 \\
&  \Leftrightarrow \dfrac{(2x+7)-2\times (3x+2)}{3x+2} \geqslant 0 \\
&  \Leftrightarrow \dfrac{2x+7-6x-4}{3x+2} \geqslant 0 \\
&  \Leftrightarrow \dfrac{3-4x}{3x+2} \geqslant 0 \\
\end{split}
\]
\'Etudions le signe du quotient $\dfrac{3-4x}{3x+2}$ à  l'aide d'un tableau de signe.

On étudie les signe de chacun des termes du quotient :
\[ 3-4x \leqslant 0 \Leftrightarrow x \geqslant  \dfrac{3}{4} \qquad \text{et} \qquad 3x+2 \leqslant 0 \Leftrightarrow x \leqslant -\dfrac{2}{3} \]
On résume dans un seul tableau le signe de chacun des termes et, on en déduit le signe du quotient en utilisant la règle des signes d'un quotient.

La double barre dans le tableau indique que $-\dfrac{2}{3}$ est une valeur interdite   :

\begin{tikzpicture}
     \tkzTabInit[espcl=3.5]
      {$x$ / 1   , $3-4x$ / 1, $-3x+2$/1, $\dfrac{3-4x}{3x+2}$/2}%
      {$\-\infty$, $-\dfrac{2}{3}$,$\dfrac{3}{4}$,$+\infty$  }%
      \tkzTabLine{,+,t,+,z,-}
      \tkzTabLine{,-,z,+,t,+}
      \tkzTabLine{,-,d,+,z,-}
    \end{tikzpicture}\\
    
    
L'ensemble des solutions de l'inéquation $\dfrac{3-4x}{3x+2} \geqslant 0$ est $S=\left]  -\dfrac{2}{3};\dfrac{3}{4} \right]$.
\end{exemple}

\textsc{Compétences}

Déterminer des images d'une fonction définie par une expression   \\
Déterminer des antécédents d'une fonction définie par une expression  \\
 Déterminer des images à partir d'un graphique \\
 Déterminer des antécédents à partir d'un graphique \\
 Donner le sens de variation d'une fonction affine \\
 Donner le tableau de signes de $ax+b$ pour des valeurs numériques données de $a$ et $b$ 
\end{document}