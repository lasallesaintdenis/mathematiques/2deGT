\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Statistiques}
\author{J.B.S. 2\up{nde}}
\date{}

\begin{document}

\maketitle

\section*{Introduction}

La statistique descriptive à laquelle nous allons nous intéresser ici
est la statistique qui permet de décrire une population en se basant sur
des critères. Entendons nous tout d'abord sur le vocabulaire à utiliser
:
\begin{description}
  \item[population] ensemble sur lequel porte notre étude ;
  \item[caractère] item étudié ;
  \item[effectif] nombre d'éléments disctincts de la population (même
    s'ils présentent le même caractère.
\end{description}

Un caractère est dit :
\begin{itemize}
  \item qualitatif si le caractère n'est pas quantifiable ;
  \item quantitatif discret lorsque le caractère ne peut prendre qu'un
    nombre dénombrable de valeurs ;
  \item quantitatif continu lorsque le caractère peut prendre toutes les
    valeurs possibles.
\end{itemize}

\marginnote{Insérer exemples ici}

\section{Caractérisation d'une série statistique}

\subsection{Introduction aux notions}

Afin de comparer plusieurs séries statistiques, on dispose de divers
notions : moyenne, médiane, variance (ou écart type), quartiles. Que
signifie ces valeurs et pourquoi sont-elles importantes. Nous allons
l'expliciter sur un exemple, quoiqu'un peu artificiel ne manquera pas de
vous rappeler une situation connue.

Considérons les séries suivantes :
\begin{itemize}
  \item $A := [ 1 ; 2 ; 3 ; 10 ; 17 ; 18 ; 19 ]$
  \item $B := [ 7 ; 8 ; 9 ; 10 ; 11 ; 12 ; 13 ]$
  \item $C := [ 7 ; 8 ; 3 ; 10 ; 17 ; 12 ; 13 ]$
\end{itemize}
Il est ici facile de vérifier que la \emph{moyenne}\footnote{total des
notes divisés par le nombre de note} de ces trois séries est 10, et que
c'est aussi la \emph{médiane}\footnote{note située «au milieu», c'est à
dire avec autant de notes avant qu'après, une fois la série ordonnée}.

Les premiers et derniers \emph{quartiles}\footnote{un quartile est le
quart des valeurs d'une série ordonnée.} des séries $A$ et $B$
diffèrent, ce qui suffit à distinguer ces deux séries. Mais, ces deux
quartiles sont identiques pour les séries $B$ et $C$. Pour cela, on peut
étudier comment les valeurs sont écartées de la moyenne. On parle alors
de \emph{variance} pour la quantité qui s'exprime comme la moyenne des
carrés des écarts à la moyenne et d'\emph{écart type} pour sa racine.

\subsection{Écriture mathématique}

Considérons désormais une série statistique
quantitative\footnote{jusqu'à la fin de cette partie, on ne peut
traiter, mathématiquement que de ces séries}de façon générale. Convenons
de la noter $X$ dans son ensemble et ses éléments $x_1$, $x_2$, $x_3$,
…, $x_n$, où $n$ est un nombre entier naturel positif non
nul\footnote{on aurait pu écrire $n\in\N^*$}, sont des nombres
réels. On a $X := [ x_1, x_2, …, x_n ]$ sans que ces éléments ne soient
nécessairement ordonnés.

\[ \overline{X} = \frac{x_1 + x_2 + x_3 + \cdots + x_n}{n} \]
Notons $x := \overline{X}$.
\[ \mathrm{Var}(X) = \frac{ (x_1 - x)^2 + (x_2 - x)^2 + (x_3 - x)^2 +
\cdots (x_n - x)^2 }{n} \]

Attention, si certaines valeurs sont présentes plusieurs fois, il faut
les compter toutes les fois. À titre d'exemple, comparer la moyenne de
$[9; 10 ; 11]$ et de $[9; 10; 11; 11]$. Cette remarque est également
valable dans le cadre de la recherche de la médiane et des quartiles.

Pour la médiane et les quartiles, la première chose à faire est
d'ordonner la série de façon croissante. Il faut désormais distinguer
deux cas : $n$ pair et $n$ impair.
\begin{itemize}
  \item Si $n$ est impair, alors $\frac{n-1}2$ et $\frac{n+1}2$ sont des
    entiers et la médiane est la moyenne des deux valeurs ayant ces
    indices sur la série ordonnée ;
  \item si $n$ est pair, alors la médiane est atteinte à l'indice
    $\frac{n}{2}$ de la série ordonnée.
\end{itemize}

Pour les quartiles, on procède de la même façon, mais sur les deux
demi-séries obtenues à l'étape précédente.

\subsection{Étude particulière}

Jusqu'à présent, nous nous sommes intéressés à la série dans son
intégralité, en prenant l'exemple d'une série générale sans aucune
structure particulière. C'est loin d'être le cas des séries qu'on étudie
et bien souvent celles-ci possèdent une structure naturelle ou une
structure qu'on leur impose.

En particulier, de nombreuses séries quantitatives discrètes présentent
des répétitions, et lorsqu'on traite une série quantitative continue, il
est d'usage d'effectuer un regroupement en classe\footnote{qu'on peut
aussi faire dans le cadre discret}.

Une fois ces regroupements effectués, on parlera d'\emph{effectif
relatif} pour un caractère. Si on divise par l'effectif total, on
parlera de \emph{fréquence} du caractère. Enfin, si au lieu de prendre
l'effectif relatif (resp. fréquence relative), on additione les
effectifs relatifs (resp. fréquences relatives) de la série ordonnée
dans l'ordre croissant, on obtient les effectifs cumulés croissants
(resp. fréquences cumulées croissantes). Et de même avec la série
ordonnée de façon décroissante.

\section{Représentations graphiques}

Un dessin vaut mieux qu'un long discours ! Mais attention, encore
faut-il qu'il soit pertinent, adapté et complet. Dans notre cas, un
graphique vaut mieux qu'une longue série statistique. Répertorions un
peu les graphiques que nous pouvons élaborer.

\subsection{Séries qualitatives ou quantitatives discrètes}

Tout d'abord, pour les séries qualitatives, on utilisera des graphiques
en bâtons, circulaire ou semi-circulaire. Explicitons un peu comment on
crée ces graphiques.

Pour le graphique en bâton, la hauteur de chaque bâton doit être
proportionnelle à l'effectif relatif considéré. Dans un graphique
circulaire (ou semi-circulaire), l'angle intercepté par la portion de
graphique doit être proportionnelle\footnote{le coefficient étant
\np{180} pour les demi-disques et \np{360} pour les disques} à la
fréquence relative considérée.

\begin{multicols}{2}
  \begin{center}
    \begin{tikzpicture}[scale=1,>=latex]
      \draw [->] (0,0) -- (5.5,0) ;
      \draw [->] (0,0) -- (0,4) ;
      \foreach \x/\y in {1/3, 2/1, 3/4, 4/0, 5/2}
      {
        \draw [thick] (\x,-0.2) node [below] {\x} -- (\x,\y) ;
      }
      \draw (0,-1) node [anchor=west] { Ce graphique correspond} ;
      \draw (0,-1.7) node [anchor=west] {à la série $[ 1 ; 1 ; 1 ; 2 ; 3; 3 ; 3; 3 ; 5
        ; 5]$.} ;
    \end{tikzpicture}
  \end{center}
  \columnbreak
  \begin{center}
    \begin{tikzpicture}
      \draw plot [smooth,domain=0:360] ({2.5*cos(\x)},{2.5*sin(\x)}) ;
      \draw [fill=green] (0,0) -- (2.5,0) -- plot [smooth,domain=0:70]
        ({2.5*cos(\x)},{2.5*sin(\x)}) -- cycle ;
      \draw (35:1.7) node [anchor=center] { D : 19\%} ;
      \draw [fill=blue] (0,0) -- plot [smooth,domain=70:150]
        ({2.5*cos(\x)},{2.5*sin(\x)}) -- cycle ;
      \draw (110:1.7) node [anchor=center] { I : 22 \% } ;
      \draw [fill=red] (0,0) -- plot [smooth,domain=150:260]
        ({2.5*cos(\x)},{2.5*sin(\x)}) -- cycle ;
      \draw (205:1.7) node [anchor=center] { C : 31 \% } ;
      \draw [fill=yellow] (0,0) -- plot [smooth,domain=260:330]
        ({2.5*cos(\x)},{2.5*sin(\x)}) -- cycle ;
      \draw (295:1.7) node [anchor=center] { E : 19\%} ;
      \draw [fill=magenta] (0,0) -- plot [smooth,domain=330:360]
        ({2.5*cos(\x)},{2.5*sin(\x)}) -- cycle ;
      \draw [<-] (345:1.7) -- (3,-0.5) node [anchor=west] { Autres :
        8\%} ;
    \end{tikzpicture}
  \end{center}
\end{multicols}

\subsection{Séries quantitatives continues}

Pour les séries quantitatives continues, il est courant d'utiliser un
histogramme, après avoir fait un regroupement en classe. Généralement on
utilise des classes identiques, mais si on utilise des classes
différentes, ce sont les aires des rectangles de l'histogramme qui
doivent refleter les propriétés de la série. Ainsi, la la largeur du
rectangle sur l'axe des abscisses sera proportionnelle à
l'\emph{amplitude}\footnote{ou étendue} de la classe et la hauteur sur
l'axe des ordonnées sera proportionnelle à l'effectif relatif à cette
classe.

On considère les valeurs suivantes qui correspondent aux tailles,
exprimées en cm d'élèves d'une classe de 2\up{de} :
172, 189, 180, 185, 162, 172, 174, 166, 190, 166, 179, 176, 160, 175,
170, 182, 160, 161, 160, 184, 181, 169, 184, 164, 177, 178, 167, 160,
176
\begin{center}
  \includegraphics[width=0.5\linewidth]{hist.png}
\end{center}

\subsection{Représentation de deux variables}

Enfin, si une série est une série à deux variables $X$ et $Y$ liées, on
peut tracer un nuage de points représentant cette série.

\end{document}
