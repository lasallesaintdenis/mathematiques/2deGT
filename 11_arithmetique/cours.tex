\chapter{Arithmétique}

\collectexercises{arithmetique}

\section{Rappels}

On a vu dans le chapitre sur les ensembles de nombres les notations
correspondant à l'ensemble des nombres naturels ($\N$) et l'ensemble des
nombres relatifs ($\Z$). On peut redonner les définitions ici.

\begin{definition}
  L'ensemble $\N$ des entiers naturels existe, contient 0 et tous les
  nombres naturels positifs, c'est-à-dire ceux qui servent à
  «compter»\footnote{On dit dénombrer}.
\end{definition}

\begin{exemple}
  \begin{itemize}
    \item $0 \in \N$
    \item $7 \in \N$
    \item $\np{567253} \in \N$
    \item $-7 \notin \N$
    \item $0,5 \notin \N$
    \item $\sqrt{2} \notin \N$.
  \end{itemize}
\end{exemple}

\begin{info}[Péano et la construction des nombres entiers naturels]
  La fin du XIXème siècle et le début du XXème siècle ont été riche en
  controverses mathématiques et philosophiques sur la nature des nombres
  (Fregge, Cantor, Dedekind, Krönecker par exemple) et sur la façon de les
  construire. Péano a apporté une réponse relativement simple, connue
  désormais sous le nom d'axiomatique de Péano qui repose sur les points
  suivants:
  \begin{itemize}
    \item existence des entiers naturels
    \item existence d'un entier de départ (noté 0)
    \item existence d'un procédé permettant de passer à l'entier suivant
    \item absence d'une borne supérieure
    \item principe de récurrence permettant d'identifier à $\N$ des
      ensembles satisfaisant une certaine propriété
  \end{itemize}
\end{info}

\begin{definition}
  L'ensemble des entiers relatifs est l'ensemble des entiers positifs et
  négatifs.
\end{definition}

\begin{remarque}
  Les éléments de l'ensemble $\Z$ se construise à partir de ceux de $\N$,
  ainsi, si $n \in \Z$, alors, soit $n \in \N$, soit $-n \in \Z$. On écrit
  donc que $\N \subset \Z$, qu'on lit $\N$ est inclus dans $\Z$.
\end{remarque}

\begin{proposition}
  Soient $n$ et $m$ deux entiers naturels.
  L'équation $x + n = m$ possède toujours une unique solution dans $\Z$.
\end{proposition}
\begin{proof}
  On raisonne par disjonction des cas:
  \begin{itemize}
    \item si $n ≤ m$, alors $m - n ≥ 0$ est une solution dans $\N \subset
      \Z$;
    \item si $n > m$, alors $ m - n < 0$ et donc la solution est dans $\Z$.
  \end{itemize}
\end{proof}

\begin{exercise}[topics = {arithmetique}]
  \begin{enumerate}
    \item Résoudre dans $\N$ l'équation $x + 3 = 2$
    \item Résoudre dans $\Z$ l'équation $x + 7 = 4$
  \end{enumerate}
\end{exercise}

\begin{definition}[division euclidienne (ou entière)]
  Soient $a$ et $b$ deux entiers naturels, avec $a > b$. Alors, il existe un
  unique couple $(q, r)$ d'entiers naturels tels que $a = bq + r, r < b$. On
  dit que $q$ est le quotient de la division de $b$ par $a$ et $r$ le teste
  de la division.
\end{definition}

\begin{exercise}[topics = {arithmetique}]
  Effectuer les divisions entières suivantes :
  \begin{itemize}
    \item $12 \div 5$
    \item $72 \div 13$
    \item $245 \div 7$
    \item $231 \div 11$
  \end{itemize}
\end{exercise}

\begin{savoirfaire}[Écrire une division entière avec Python]
  \begin{pyconsole}
81 // 7
81 % 7
11 * 7 + 4 == 81
4 < 7
  \end{pyconsole}
\end{savoirfaire}

\begin{remarque}
  L'algorithme suivant n'est pas connaître, mais peut servir d'algorithme
  naïf de la division.
\end{remarque}
\begin{pyblock}
def divise(a, b):
    if a < b:
        return (0,b)
    elif a == b:
        return (a,0)
    else:
        q = 0
        r = a
        while r > b:
            q = q + 1
            r = r - b
    return (q,r)


\end{pyblock}
\begin{pysub}
divise(81,7) renvoie !{divise(81,7)}
\end{pysub}

\section{Notions élémentaires}

L'objet de ce cours est de donner des notions très élémentaires
d'arithmétique, mais qui pourront être étendues. En particulier les notions
vues ici s'appliquent aussi bien à des entiers positifs qu'à des entiers
relatifs.

\subsection{Multiples et diviseurs}

\begin{definition}
  Soient $a$ et $n$ deux entiers relatifs. L'ensemble des nombres $\{ b | b
  = na,\ n \in\Z \}$ est l'ensemble des multiples de $a$.
\end{definition}

\begin{exercise}[topics = {arithmetique}]
  \begin{enumerate}
    \item Donner les 5 premiers multiples positifs de 2.
    \item Donner les 3 premiers multiples négatifs de 3.
  \end{enumerate}
\end{exercise}

\begin{exercise}[topics = {arithmetique}]
  \begin{enumerate}
    \item $55$ est-il un multiple de 11 ?
    \item $24$ est-il un multiple de 4 ?
    \item $78$ est-il un multiple de 6 ? de 3 ?
  \end{enumerate}
\end{exercise}

\begin{proposition}
  La somme de deux multiples d'un même nombre $a$ est un multiple de $a$.
\end{proposition}
\begin{proof}
  \leavevmode

  \noindent\ligne{4}
\end{proof}

\begin{exercise}[topics = {arithmetique}]
  \begin{enumerate}
    \item Justifier que $240$ est un multiple de 3.
    \item En déduire que $252$ est un multiple de 3.
  \end{enumerate}
\end{exercise}

\begin{savoirfaire}[Détermination de multiples]
  \begin{multicols}{2}
    \begin{pyblock}
def est_multiple(a, b):
    if a == b:
        return True
    elif a < b:
        if b % a == 0:
            return True
    else:
        if a % b == 0:
            return True
    return False
    \end{pyblock}
    \columnbreak
    \begin{pyblock}
def plus_grand_multiple (a, b):
    if a == b:
        return a
    elif a > b:
        return 0
    else:
        m = 0
        while m*a < b:
            if m*a < b:
                m = m + 1
            else:
                return m*a
    \end{pyblock}
  \end{multicols}
\end{savoirfaire}

\begin{definition}[diviseur]
  Soient $a$ et $b$ deux nombres entiers relatifs. On dit que $a$ divise $b$
  (noté $a | b $) lorsque $b$ est un multiple de $a$, c'est-à-dire qu'il
  existe $k \in \Z$, tel que $b = ka$.
\end{definition}

\begin{exercise}[topics = {arithmetique}]
  \begin{enumerate}
    \item Donner les diviseurs de 12.
    \item Donner les diviseurs de 60.
    \item Donner les diviseurs de 45
  \end{enumerate}
\end{exercise}

En pratique, pour des entiers positifs, on utilise souvent la
caractérisation par le reste de la division.

\begin{proposition}
  Soient $a$ et $b$ deux entiers naturels non nuls. On dit que $a$ divise
  $b$ lorsque le reste de la division de $b$ par $a$ est nul.
\end{proposition}
\begin{proof}
  Si le reste $r$ est tel que $r = 0$, alors $b = qa$, ce qui correspond à
  la définition.
\end{proof}

\begin{exemple}
  \leavevmode
  \begin{pyconsole}
231 % 3
347 % 7
  \end{pyconsole}
\end{exemple}

\begin{definition}[nombres premiers]
  On dit qu'un nombre supérieur ou égal à 2 est \emph{premier} s'il n'admet
  comme seuls diviseurs que 1 et lui-même.
\end{definition}

\begin{exercise}[topics = {arithmetique}]
  Parmi cette liste de nombre, indiquer quels sont les nombres premiers : \[
  3 ; 5; 12 ; 14 ; 18 ; 19 \]
\end{exercise}

\subsection{Nombres pairs ou impairs}

\begin{definition}
  \begin{itemize}
    \item Un nombre entier divisible par deux est \emph{pair} ;
    \item un nombre entier non divisible par deux est \emph{impair}
  \end{itemize}
\end{definition}

\begin{remarque}
  On écrit souvent pour les nombres pairs qu'ils sont de la forme $n = 2p$
  avec $p \in \Z$ et pour les nombres impairs qu'ils sont de la forme $n =
  2p + 1$ avec $p \in \Z$.
\end{remarque}

\begin{exercise}[topics = {arithmetique}]
  Donner un critère simple permettant de déterminer si un nombre donné est
  divisible par 2.
\end{exercise}

\begin{proposition}
  La somme de deux nombres pairs est un nombre pair.
\end{proposition}
\begin{proof}
  Soit $n$ et $m$ deux nombres pairs. Alors il existe deux entiers $p$ et
  $q$ tels que $ n = 2p$ et $m = 2q$.\\
  $n + m = 2p + 2q = 2(p+q)$ qui est bien un multiple de deux.
\end{proof}

\begin{proposition}
  La somme de deux nombres impairs est un nombre pair.
\end{proposition}
\begin{proof}
  Soit $n$ et $m$ deux nombres impairs. Alors il existe deux entiers $p$ et
  $q$ tels que $ n = 2p + 1$ et $m = 2q + 1$.\\
  $n + m = 2p + 1 + 2q + 1 = 2(p+q) + 2 = 2(p + q + 1)$ qui est bien un
  multiple de deux.
\end{proof}

\begin{proposition}
  La somme d'un nombre impair et d'un nombre pair est un nombre impair.
\end{proposition}
\begin{proof}
  Soit $n$ et $m$ deux nombres l'un pair et l'autre impair.
  Alors il existe deux entiers $p$ et $q$ tels que $ n = 2p + 1 $ et $m = 2q
  $.\\
  $n + m = 2p + 1 + 2q = 2(p+q) + 1 $ qui est bien un multiple de deux plus un et est donc bien impair.
\end{proof}

\begin{exercise}[topics={arithmetique}]
  Démontrer les propositions suivantes
  \begin{enumerate}
  %\begin{proposition}
    \item Le produit de deux nombres pairs est un nombre pair.
  %\end{proposition}

  %\begin{proposition}
    \item La somme d'un nombre impair et d'un nombre pair est un nombre
      impair.
  %\end{proposition}

  %\begin{proposition}
    \item Le produit de deux nombres impairs est un nombre impair.
  %\end{proposition}
  \end{enumerate}
\end{exercise}

\begin{proposition}
  Le carré d'un nombre impair est impair.
\end{proposition}
\begin{proof}
  \leavevmode

  \noindent\ligne{6}
\end{proof}

\section{Applications}

\begin{savoirrediger}[Rédiger avec la contraposée]
  Lorsqu'on a une proposition de la forme «Si A alors B», où $A$ et $B$
    sont des assertions mathématiques pouvant être vraie ou fausse (on
    note aussi $A \implies B$), alors on aussi «Si non B alors non B»
    (ce qu'on note $\not A \implies \not B$.)
    %TODO : trouver un exemple.
\end{savoirrediger}[Rédiger avec la contraposée]
Lorsqu'on a une proposition de la forme «Si A alors B», où $A$ et $B$ sont des assertions mathématiques pouvant 
\begin{proposition}
  Le nombre $\sqrt{2} \notin  \Q$.
\end{proposition}
\begin{proof}
  \leavevmode

  \noindent\ligne{12}
\end{proof}

\begin{savoirfaire}[Détermination naïve de la primalité]
  \begin{pyblock}
from math import sqrt
def est_premier(n):
    if n < 2:
        return False
        for i in range(int(sqrt(n))):
            if n % i == 0:
                return True
    return False


est_premier(2)
est_premier(5)
est_premier(10)
  \end{pyblock}

  \pys{!{est_premier(2)}}
\end{savoirfaire}

\collectexercisesstop{arithmetique}

\printcollection{arithmetique}
\printcollection{all exercises}
