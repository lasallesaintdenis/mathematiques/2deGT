\chapter{Probabilités}

\section{Probabilités sur un ensemble fini}

\subsection{Expérience aléatoire}

\begin{definition}
	Une expérience est dit \textbf{aléatoire} lorsqu'elle possède plusieurs \textbf{issues} possibles et qu'on ne peut ni prévoir ni calculer laquelle de ces issues sera réalisée.
\end{definition}

\begin{info}[Notation des issues]
	L'ensemble des issues est souvent noté $\Omega = \set{x_1 ; x_2 ; … ; x_n}$. On note les parties de $\Omega$ avec des lettres majuscules.\\
	$\Omega$ s'appelle l'\textbf{univers} des possibles.
\end{info}

\subsection{Modélisation d'une expérience aléatoire}

\begin{definition}\label{probabilite:def:axiomes}
	\textbf{Modéliser} une \textbf{expérience aléatoire}, c'est associer à chaque issue $x_i$ possible une valeur $p_i$, appelée \textbf{probabilité} de $x_i$, tel que
	\begin{center}
		\begin{itemize*}
			\item Pour tout $x_i \in \Omega,\ 0 ≤ p_i(x_i) ≤ 1$ \hfill~
			\item $p_1 + p_2 + … + p_n = 1$.\hfill~
		\end{itemize*}
	\end{center}
\end{definition}

\begin{proposition}[Loi des grands nombres]
	\leavevmode
	\begin{itemize}
		\item Si on connaît un modèle adapté à une expérience aléatoire et qu'on répète un grand nombre de fois cette même expérience aléatoire, alors les fréquences de réalisations des issues se rapprochent de la probabilité.
		\item Réciproquement, on peut élaborer un modèle en répétant un grand nombre de fois la même expérience aléatoire et en assimilant les fréquences observées aux probabilités.
	\end{itemize}
\end{proposition}

\subsection{Le modèle d'équiprobabilité}

\begin{definition}
	Dans une expérience aléatoire, si toutes les issues ont la même probabilité, on dit alors qu'il y a \textbf{équiprobabilité}.
\end{definition}

\begin{exemple}
	\leavevmode
	\begin{itemize}
		\item Lancer un dé \textbf{équilibré},
		\item lancer une pièce \textbf{non truquée},
		\item choisir une boule \textbf{indiscernable au toucher dans un sac opaque}
	\end{itemize}
	sont des expériences aléatoires aux issues \textbf{équiprobables}.
\end{exemple}

\begin{proposition}
	Soit une expérience aléatoire équiprobable.\\
	Si cette expérience possède $n$ issues possibles, alors $p_i = p = \frac1n$.
\end{proposition}
\begin{proof}
	D'après la définition~\ref{probabilite:def:axiomes}, on sait que la somme des probabilités vaut $1$ et comme elles sont toutes égales à $p$, on en déduit que $p×n = 1$.
\end{proof}

\section{Probabilité d'un événement}

\subsection{Notion d'événement}

\begin{definition}
	Un événement $A$ est un sous-ensemble (ou une partie) de l'univers $\Omega$ d'une expérience aléatoire.
\end{definition}

\begin{savoirfaire}[Représenter un événement]
	\begin{center}
		\begin{tikzpicture}
			\draw (0,0) ellipse (2cm and 1cm);
			\draw (0,0) node { $A = \set{x_1,x_2}$ };
			\draw (0.5,0.5) ellipse (4cm and 2 cm);
			\draw (3.5,1.5) node { $\Omega$ } ;
		\end{tikzpicture}
	\end{center}
\end{savoirfaire}

\begin{note}[Vocabulaire et notations]
	\begin{itemize}
		\item Dire qu'une issue $x_i$ \textbf{réalise} l'événement $A$, c'est dire que $x_i \in A$.
		\item Une partie $\set{x_i}$ qui ne contient que $x_i$ est appelé \textbf{singleton}. L'événement $A = \set{x_i}$ est dit \textbf{élémentaire}.
		\item L'ensemble vide, noté $ø$ signifie qu'aucune issue n'appartient à cet événement. On parle d'\textbf{événement impossible}.
		\item $\Omega$ est l'\textbf{événement certain}.
	\end{itemize}
\end{note}

\begin{savoirrediger}[Détailler différents événements]
	On lance un dé à 6 faces, numérotées de $1$ à $6$.
	\begin{itemize}
		\item «Obtenir un numéro inférieur ou égal à $2$ est l'événement constitué de $\set{1;2}$.
		\item «Obtenir $7$» est un événement impossible.
	\end{itemize}
\end{savoirrediger}

\subsection{Probabilité d'un événement}

\begin{definition}
	La probabilité de l'événement $A$ est la somme de toutes les probabilités des issues qui constituent $A$. On la note $p(A)$.
\end{definition}

\begin{proposition}
	\leavevmode
	\begin{itemize}
		\item $p(\Omega) = 1$ ;
		\item $p(ø) = 0$ ;
		\item pour tout événement $A$, $0 ≤ p(A) ≤ 1$.
	\end{itemize}
\end{proposition}
\begin{proof}
	$\Omega$ étant constitué de toutes les issues, en utilisant la définition~\ref{probabilite:def:axiomes}, on obtient le résultat.\\
	$ø$ étant vide, la seule solution possible est $p(ø) = 0$.\\
	Enfin, si $A$ est un événement différent de $ø$ et $\Omega$, il contient une partie des $x_i$. Chacun d'entre-eux est tel que $0 ≤ p_i(x_i) ≤ 1$ et $p_1 + p_2 + … + p_n = 1$. Si on en retire quelques uns de l'égalité précédente, le résultat doit être inférieur à 1.\\
	De plus, comme il sont tous positifs, la somme de quelques uns d'entre eux est nécessairement positive. Donc $0 < p(A) < 1$.\\
	Si $A = ø$ ou $A = \Omega$, on en déduit finalement $0 ≤ p(A) ≤ 1$.
\end{proof}

\begin{savoirrediger}[Donner les probabilités des issues]
	On considère un dé truqué, pour lequel on donne les probabilités :
	\begin{center}
		\begin{tabular}{|l|*{6}{c|}}\hline
			Valeur du dé & $1$ & $2$ & $3$ & $4$ & $5$ & $6$ \\ \hline
			Probabilité	 & $\frac1{12}$ & $\frac14$ & $\frac1{12}$ & $\frac16$ & $\frac14$ & $\frac16$ \\ \hline
		\end{tabular}
	\end{center}
	On vérifie que $\frac1{12} + \frac14 + \frac1{12} + \frac16 + \frac14 + \frac16 = 1$
\end{savoirrediger}

\begin{proposition}
	Dans une expérience aléatoire où il y a équiprobabilité, on peut calculer $p(A)$ en utilisant \[ p(A) = \frac{\text{nombre d'issues dans }A}{\text{nombre total d'issues}} . \]
\end{proposition}
\begin{proof}
	Comme il y a équiprobabilité, la probabilité de chaque issue est $\frac1n$, où $n$ est le nombre total d'issues.\\
	Si $m$ issues réalisent $A$, alors, $p(A) = m×\frac1n = \frac{m}{n}$.
\end{proof}

\section{Calcul des probabilités}

\subsection{Union et intersection d'événements}

\begin{definition}
	Soient $A$ et $B$ deux parties de $\Omega$.
	\begin{itemize}
		\item L'\textbf{intersection}  de $A$ et $B$ est l'événement formé des issues qui réalisent à la fois $A$ \textcolor{red}{et} $B$.\\
		On la note $A$ \textcolor{red}{$\cap$} $B$ et on lit «$A$ \textcolor{red}{inter} $B$».
		\item L'\textbf{union}  de $A$ et $B$ est l'événement formé des issues qui réalisent $A$ \textcolor{blue}{ou} $B$.\\
		On la note $A$ \textcolor{blue}{$\cup$} $B$ et on lit «$A$ \textcolor{blue}{union} $B$».
	\end{itemize}
\end{definition}

\begin{multicols}{2}
	\begin{center}
		\textbf{Intersection}
		
		\begin{tikzpicture}
			\begin{scope}
				\clip (0,0) ellipse (2cm and 1cm);
				\fill[red] (1,1) ellipse [x radius = 2, y radius = 1, rotate = 30] ;
			\end{scope}
			
			\draw (0,0) ellipse (2cm and 1cm);
			\draw (-0.25,-0.55) node { $A = \set{x_1,x_2}$ };
			\draw (0.5,0.5) ellipse (4cm and 2 cm);
			\draw (3.5,1.5) node { $\Omega$ } ;
			\draw (1,1) ellipse [x radius = 2, y radius = 1, rotate = 30] ;
			\draw (1.2,1.2) node { $B = \set{x_2,x_3,x_4} $ } ;
		\end{tikzpicture}
		
		$A \cap B = \set{x_2}$
	\end{center}
	\begin{center}
		\textbf{Union}
		
		\begin{tikzpicture}
			\draw [fill,blue!50] (0,0) ellipse (2cm and 1cm) (1,1) ellipse [x radius = 2, y radius = 1, rotate = 30] ;
	
			\draw (0,0) ellipse (2cm and 1cm);
			\draw (-0.25,-0.55) node { $A = \set{x_1,x_2}$ };
			\draw (0.5,0.5) ellipse (4cm and 2 cm);
			\draw (3.5,1.5) node { $\Omega$ } ;
			\draw (1,1) ellipse [x radius = 2, y radius = 1, rotate = 30] ;
			\draw (1.2,1.2) node { $B = \set{x_2,x_3,x_4} $ } ;
		\end{tikzpicture}
	\end{center}

			$A \cup B = \set{x_1,x_2,x_3,x_4}$
\end{multicols}

\begin{note}[Cas d'événements incompatibles]
	Lorsque deux événements $A$ et $B$ sont incompatibles ($A \cap B = ø$), on a alors $p(A \cup B) = p(A) + p(B)$.
		\begin{center}
			\begin{tikzpicture}
			\draw (-.8,0) ellipse (1.75cm and 1cm);
			\draw (-0.65,-0.45) node { $A = \set{x_1,x_2}$ };
			\draw (0.5,0.5) ellipse (4cm and 2 cm);
			\draw (3.5,1.5) node { $\Omega$ } ;
			\draw (2.1,1) ellipse [x radius = 2, y radius = 0.85, rotate = 150] ;
			\draw (1.9,1.2) node { $B = \set{x_3,x_4} $ } ;
		\end{tikzpicture}
	\end{center}
\end{note}

\subsection{Formule de calcul}

\begin{proposition}\label{probabilites:prop:proba_intersection}
	Soient $A$ et $B$ deux événements.\\
	Alors \[ p(A \cap B) = p(A) + p(B) - p(A \cup B) . \]
\end{proposition}
\begin{proof}
	~\\[2ex]
	\begin{tikzpicture}
		\draw[fill,blue!50] (0,0) ellipse (2cm and 1cm);
		\begin{scope}
			\clip (0,0) ellipse (2cm and 1cm);
			\fill[red] (1,1) ellipse [x radius = 2, y radius = 1, rotate = 30] ;
		\end{scope}
	
		\draw (0,0) ellipse (2cm and 1cm);
		\draw (-0.25,-0.55) node { $A_1$ };
		\draw (0.5,0.5) ellipse (4cm and 2 cm);
		\draw (3.5,1.5) node { $\Omega$ } ;
		\draw (1,1) ellipse [x radius = 2, y radius = 1, rotate = 30] ;
		\draw (1.2,1.2) node { $B$ } ;
	\end{tikzpicture}
	
	\noindent On note \textcolor{blue!50}{$A_1$} l'événement formé des issues de $A$ qui ne sont pas dans $B$. $A_1$ et $A \cap B$ sont incompatibles et de réunion $A$ donc $p(A) = p(A_1) + p(A \cap B)$.\\
	$A_1$ et $B$ sont incompatibles et de réunion $A \cup B$. Donc $p(A \cup B) = p(A_1) + p(B)$. Ainsi,\\
	$p(A_1) = p(A) - p(A \cap B)$ et en remplaçant, on obtient \\
	$p(A \cup B) = p(A) - p(A \cap B) + p(B)$.
\end{proof}

\subsection{Événement contraire}

\begin{definition}
	Soit $A$ un événement.\\
	L'événement contraire est formé des éléments qui n'appartiennent pas à $A$. On le note $\overline{A}$.
\end{definition}

\begin{note}[Représentation de l'événement contraire]
		\begin{center}
		\begin{tikzpicture}
		\draw [fill,blue!25] (0.5,0.5) ellipse (4cm and 2 cm);
		\draw [fill,white] (0,0) ellipse (2cm and 1cm);
		\draw (-2.5,0.5) node {$\overline{A}$} ;
		\draw (0,0) ellipse (2cm and 1cm);
		\draw (0,0) node { $A$};
		\draw (0.5,0.5) ellipse (4cm and 2 cm);
		\draw (3.5,1.5) node { $\Omega$ } ;
		\end{tikzpicture}
	\end{center}
\end{note}

\begin{proposition}
	Soit $A$ un événement.\\
	Alors la probabilité de l'événement contraire est $p(\overline{A}) = 1 - p(A)$.
\end{proposition}
\begin{proof}
	$A$ et $\overline{A}$ sont des événements incompatibles et leur réunion $A \cup \overline{A} = \Omega$. D'après la proposition~\ref{probabilites:prop:proba_intersection}, on a $p(\Omega) = p(A) + p(\overline{A})$, d'où\\
	$p(\overline{A}) + p(A) = 1$ et donc\\
	$p(\overline{A}) = 1 - p(A)$.
\end{proof}

\begin{info}[De Pascal et Fermat à Lebesgue et Kolmogorov]
	Si la théorie des probabilités a pris ses origines dans les jeux de hasard, devenant ainsi une discipline mathématique peu fréquentable, c'est que les premiers exemples pris pour illustrer les résultats concernait les gains et les pertes à des jeux de hasards.\\
	Cependant, une théorie plus complète a vu le jour au tournant du XX\ieme{} siècle, lui donnant des lettres de noblesses.\\
	Aujourd'hui, les probabilités servent d'outil d'analyse pour tous les processus physiques ou sociaux qui ne peuvent être, pour l'instant, mis en équation dans le cadre d'un modèle ou d'une théorie.\\
	Les probabilités sont également utiles dans le cadre de la théorie des sondages.
\end{info}
