\chapter{Statistique descriptive}

\section{Vocabulaire}
L’ensemble sur lequel porte l’étude statistique s’appelle la \textcolor{red}{\textit{population}}.\\
Un élément de cet ensemble est un \textcolor{red}{\textit{individu}}.\\
La population est étudiée selon un ou plusieurs \textcolor{red}{\textit{caractères}}.\\
Un caractère permet de déterminer une partition de la population selon ses diverses valeurs ( par exemple le sexe est un caractère à deux valeurs : masculin ou féminin).\\
Lorsque les valeurs d’un caractère sont des nombres, le caractère est dit \textcolor{red}{ \textit{quantitatif}}. \\
Lorsque les valeurs du caractère ne sont pas mesurables, le caractère est dit \textcolor{red}{\textit{qualitatif}}.\\
L’\textcolor{red}{\textit{effectif }}d’une classe statistique est le nombre d’éléments de la population observés dans cette classe.\\
La \textcolor{red}{\textit{fréquence}} d’une classe statistique est le rapport de l’effectif de cette classe à l’effectif total de la population. ( la fréquence peut être exprimée en pourcentage )

\begin{center}
fréquence = $\dfrac{\textbf{effectif}}{\textbf{effectif total}}$
\end{center}
\section{Caractéristiques}
\subsection{Moyenne arithmétique}

La \textcolor{red}{\emph{moyenne arithmétique}} d'une série statistique quantitative $S=\{x_1,x_2,\ldots,x_n\}$ est le nombre,
note $\overline{x}$ : \[\overline{x}=\frac{x_1+x_2+\ldots+x_n}{n}\]

\textbf{Remarque:\\}
\begin{itemize}
\item De la définition, on peut déduire que $n\overline{x}=x_1+x_2+\ldots+x_n$, ce qui peut s'interpréter de la manière suivante : \og La somme de toutes les valeurs de la série est inchangée si on remplace chaque valeur par $\overline{x}$ \fg. 
\item Si la série $S$ comporte $n$ données $x_1, x_2, \ldots, x_p$ d'effectifs respectifs (ou de fréquences respectives) $n_1$, $n_2$, \ldots, $n_p$,
alors $\overline{x}=\frac{n_1x_1+n_2x_2+\ldots+n_px_p}{\underbrace{n_1+n_2+\ldots+n_p}_{\text{effectif total}}}$
\end{itemize}


\subsection{M\'ediane}

On appelle \emph{médiane} d'une série statistique quantitative tout nombre $m$ tel que :
\begin{itemize}
	\item la moitié au moins des valeurs de la série est inférieure à $m$
	\item la moitié au moins des valeurs de la série est supérieure à $m$\\
\end{itemize}

\textbf{Remarque : \\}
\begin{itemize}
	\item Rappel : mathématiquement \og inférieur \fg{} et \og supérieur \fg{} signifient, en français, \og inférieur ou égal \fg{} et \og supérieur ou égal \fg.
	\item On admettra qu'un tel nombre existe toujours.
	\item La médiane partage la série en deux sous-séries ayant \emph{quasiment} le même effectif ; \emph{quasiment} car si plusieurs valeurs de la série sont égales à la médiane, les données inférieures à la médiane et les données supérieures à la médiane ne seront pas forcément en nombre égal.
	\item Il faut comprendre la médiane comme \og la valeur du milieu \fg.\\
\end{itemize}

Soit une série statistique quantitative comportant $n$ données : $S=\{x_1,x_2,\ldots,x_i,\ldots,x_n\}$ telles que $x_1\leqslant x_2\leqslant \ldots \leqslant x_n$.
\begin{itemize}
	\item Si $n$ est impair, la $\frac{n+1}{2}^{\text{ième}}$ donnée de la série est la médiane.
	\item Si $n$ est pair, tout nombre compris entre le $\frac{n}{2}^{\text{ième}}$ élément de la série et le suivant est \textbf{une} médiane ; dans le cadre scolaire \textbf{la} médiane sera la moyenne des deux données centrales de la série :
	      \[m=\frac{\left(\frac{n}{2}\right)^{\text{ième}}+\left(\frac{n}{2}+1\right)^{\text{ième}}}{2}\]
\end{itemize}

Exemple 1 : \\
\begin{center}
\begin{tabular}{|*{5}{c|}}
\hline
Couleurs & Blonds& Bruns & Châtains & Roux \\ \hline
effectifs & 25 &57 &91 &23 \\ \hline
\end{tabular}
Paramètres : $\overline{x}=1,85 ; med=2 ; Q_1=1 ; Q_3=3 $

\end{center}


Exemple 2 : \\
\begin{center}
\begin{tabular}{|*{4}{c|}}
\hline
Salaires & $[1\,000 \,;\, 2\,000[$ & $[2\,000 \,;\, 3\,000[$ & $[3\,000 \,;\, 4\,000[$ \\ \hline
Ouvriers & 114 & 66 & 20 \\ \hline
Centre de classe & 1 100 & 2 500& 3 500\\ \hline
\end{tabular}
\end{center}
Paramètres : $\overline{x}=1802 ; med=1100 ; Q_1=1100 ; Q_3=2500 $

Exemple 3 : 
\begin{center}
\begin{tabular}{|*{22}{c|}}\hline
Valeurs $x_i$ & 0 & 1 & 2& 3 & 4& 5\\ \hline
Effectifs $n_i$ & 3 & 5 & 6& 5 & 0& 1\\ \hline
\end{tabular}
\end{center}

Paramètres : $\overline{x}=1,85 ; med=2 ; Q_1=1 ; Q_3=3 $
\section{Mesures de dispersion}

 Elles visent à indiquer comment les données de la série statistique sont dispersées par rapport aux mesures centrales.

\subsection{Valeurs extr\^emes}

Les valeurs extrêmes d'une série quantitative sont ses valeurs \emph{minimale} et \emph{maximale} et l'\emph{étendue} est la différence entre les valeurs extrêmes de la série.

\subsection{Quartiles}

Quartiles dans le cas g\'en\'eral
Soit $S$ une série statistique quantitative.

\begin{itemize}
	\item On appelle \emph{premier quartile}, noté $Q_1$, tout réel tel que
			\begin{itemize}
				\item au moins 25\% des valeurs de la série ont une valeur inférieure ou égale à $Q_1$
			\item
			au moins 75\% des valeurs de la série ont une valeur supérieure ou égale à $Q_1$
			\end{itemize}
	\item On appelle \emph{deuxième quartile}, noté $Q_2$, tout réel tel que
			\begin{itemize}
				\item au moins 50\% des valeurs de la série ont une valeur inférieure ou égale à $Q_2$
			\item
			au moins 50\% des valeurs de la série ont une valeur supérieure ou égale à $m$
			\end{itemize}
		\item On appelle \emph{troisième quartile}, noté $Q_3$, tout réel tel que
			\begin{itemize}
				\item au moins 75\% des valeurs de la série ont une valeur inférieure ou égale à $Q_3$
			\item
			au moins 25\% des valeurs de la série ont une valeur supérieure ou égale à $Q_3$
			\end{itemize}
\end{itemize}

\begin{itemize}
 \item $Q_2$ est, par d\'efinition, la m\'ediane de la s\'erie.
 \item On admettra que de tels nombres existent toujours.
 \item La m\'ediane partage une s\'erie en deux sous-s\'eries ayant quasiment le m\^eme effectif (environ 50\,\%) ; les premier, troisi\`eme quartiles et la m\'ediane partageront une s\'erie en quatre sous-s\'eries ayant quasiment le m\^eme effectif (environ 25\,\%). \\
\end{itemize}

\textbf{Proposition\\}
Soit une série statistique quantitative comportant $n$ données : $S=\{x_1,x_2,\ldots,x_i,\ldots,x_n\}$ telles que $x_1\leqslant x_2\leqslant\ldots \leqslant x_n$. Alors :
\begin{itemize}
	\item La donn\'ee de rang $\frac{1}{4}n$ (arrondi au sup\'erieur si ce n'est pas un entier) convient toujours comme premier quartile.
	\item La donn\'ee de rang $\frac{3}{4}n$ (arrondi au sup\'erieur si ce n'est pas un entier) convient toujours comme troisi\`eme quartile.\\
	\end{itemize}

\textbf{Exemple\\}
\begin{itemize}
 \item S'il y a $n=29$ données dans la série, rangées dans l'ordre croissant :
\begin{itemize}
	\item $\frac{1}{4}\times29=7,25\approx 8$
	donc la huitième donnée de la série convient comme premier quartile ;
	\item $\frac{3}{4}\times29=21,75\approx 22$
	donc la vingt-deuxième donnée de la série convient comme troisième quartile.
	\end{itemize}
 \item S'il y a $n=64$ donn\'ees dans la s\'erie, rang\'ees dans l'ordre croissant : 
\begin{itemize}
	\item $\frac{1}{4}\times64=16$
	donc la seizi\`eme donnée de la série convient comme premier quartile ;
	\item $\frac{3}{4}\times64=48$
	donc la quarante huiti\`eme donnée de la série convient comme troisième quartile.\\
	\end{itemize}
\end{itemize}

\textbf{Proposition\\}
Soit une série statistique quantitative comportant $n$ données : $S=\{x_1,x_2,\ldots,x_i,\ldots,x_n\}$ avec $n\geqslant5$.

On ne change les déciles, les quartiles et la médiane si on remplace $x_1$ par n'importe quel nombre de l'intervalle $]-\infty\,;\,x_1]$ et $x_n$ par n'importe quel nombre de l'intervalle $[x_n\,;\,+\infty[$.


Comme on ne change pas le nombre de valeurs de la série, il y en aura toujours autant inférieures et supérieures à $D_1$, $Q_1$, $m$, $Q_3$ et $D_9$.


Une fois les premier et troisi\`eme quartiles disponibles, on d\'efinit l'\'ecart et l'intervalle interquartile de la mani\`ere suivante :

Définition\\
 Soit $S$ une s\'erie statistique quantitative et $Q_1$ et $Q_3$ ses premier et troisi\`eme quartiles. On appelle :
 \begin{itemize}
  \item \emph{écart interquartile} la différence $Q_3 - Q_1$ ;
  \item \emph{intervalle interquartile} l'intervalle $[Q_1 \,;\, Q_3]$.
 \end{itemize}



\section{Représentations graphiques}

Si les mesures centrales et les mesures de dispersion ont pour but de r\'esumer une s\'erie statistique en quelques nombres, les repr\'esentations graphiques, elles, visent \`a la visualiser.

\subsection{Diagramme à bâtons}

On considère la série :
%\vspace{-1em}
\begin{center}
\begin{tabular}{|*{22}{c|}}\hline
Valeurs $x_i$ & 0 & 1 & 2& 3 & 4& 5& 6\\ \hline
Effectifs $n_i$ & 3 & 5 & 6& 5 & 6& 7& 7\\ \hline
\end{tabular}
\end{center}

\begin{center}
\begin{tikzpicture}
  \tkzInit[xmin=0,xmax=6,ymin=0,ymax=7]
    \tkzAxeX[]
        \tkzAxeY[]
        \draw [very thick, blue] (0,0)--(0,3);        
        \draw [very thick, blue] (1,0)--(1,5);
        \draw [very thick, blue] (2,0)--(2,6);
        \draw [very thick, blue] (3,0)--(3,5);
        \draw [very thick, blue] (4,0)--(4,6);
        \draw [very thick, blue] (5,0)--(5,7);
        \draw [very thick, blue] (6,0)--(6,7);
\end{tikzpicture}
\end{center}


\subsection{Diagrammes bas\'es sur la fr\'equence}

 Les s\'eries statistiques peuvent aussi \^etre repr\'esent\'ees en diagrammes circulaires, semi-circulaires, rectangulaires, etc.
L'aire de chaque modalit\'e devra \^etre proportionnelle \`a l'effectif de cette modalit\'e.
Les fr\'equences permettent d'obtenir assez facilement la part du diagramme qui devra \^etre consacr\'ee \`a chaque modalit\'e.

 Ainsi si on consid\`ere la s\'erie suivante :
\begin{center}
\begin{tabular}{|*{5}{c|}}\hline
Donn\'ees & Blonds 	& Bruns & Ch\^atains & Roux  \\ \hline
Effectifs $n_i$ & 25		& 57		&91		&23 \\ \hline
\end{tabular}
\end{center}            

 On obtient les diagrammes de la figure\\
\begin{center}
 \begin{tikzpicture}
\draw[fill=red] (0,0)--(0:2.5)arc(0:52.2:2.5)--cycle;
\draw[fill=green] (0,0)--(52.2:2.5)arc(52.2:154.8:2.5)--cycle;
\draw[fill=blue] (0,0)--(154.8:2.5)arc(154.8:196.2:2.5)--cycle;
\draw[fill=violet] (0,0)--(196.2:2.5)arc(196.2:360:2.5)--cycle;
 \end{tikzpicture}
\end{center}



\subsection{Diagramme en boite}

 On peut représenter graphiquement les valeurs extrêmes, les quartiles et la médiane par un \emph{diagramme en boite}, appelé aussi \emph{boite à moustaches}, conçu de la manière suivante :
\begin{itemize}
	\item au centre une boite allant du premier au troisième quartile, séparée en deux par la médiane ;
	\item de chaque côté une moustache allant du minimum au premier quartile pour l'une, et du troisième quartile au maximum pour l'autre.
\end{itemize}

Ce type de diagramme permet une interprétation visuelle et rapide de la dispersion des séries statistiques. Il
permet également d'apprécier des différences entre des séries (lorsqu'elles ont des ordres de grandeurs
comparables).

\begin{tikzpicture}[scale=0.9]
\tkzInit[xmin=0,xmax=20]
\tkzAxeX
\begin{scope}[xshift= 5cm, yshift=0.5cm]
\draw rectangle(2,1) node[above] {Médiane};
\draw rectangle(10,1);
\end{scope}
\draw (2,1) node{$\bullet$} node[above]{min} -- (5,1) node {$\bullet$} node[right]{$Q_1$};
\draw (15,1)node {$\bullet$} node[left]{$Q_3$} -- (19,1)node{$\bullet$} node[above]{max};
\end{tikzpicture}

Remarque\\
\begin{itemize}
	\item La hauteur des boites est arbitraire (on les fait parfois proportionnelles à l'effectif total de la série).
	\item La boite contient les 50\% des données centrales.

\end{itemize}


