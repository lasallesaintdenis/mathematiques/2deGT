\chapter{Fonctions affines}

\section{Généralités sur les fonctions affines}

\subsection{Définition}

\begin{definition}
	Soient $a$ et $b$ deux nombres réels.\\
	La fonction $f$, qui à tout $x$ réel, associe $f(x) = a×x + b$ s'appelle fonction affine.
\end{definition}

\begin{remarque}
	\leavevmode
	\begin{itemize}
		\item Si $a = 0$, la fonction $f$ devient la fonction constante $f \colon x \mapsto b$. Ce point sera démontré à la proposition~\ref{fonctions_affines:prop:variations}.
		\item Si $b = 0$, la fonction $f$ devient la fonction linéaire $f \colon x \mapsto a×x$.
		\item Si $a = 0$ et $b = 0$, la fonction $f$ devient la fonction nulle $f \colon x \mapsto 0$.
	\end{itemize}
\end{remarque}

\begin{exemple}
	\leavevmode
	\begin{itemize}
		\item $f(x) = -3x + 2$
		\item $g \colon x \mapsto \frac12 x - 3$
	\end{itemize}
\end{exemple}

\begin{proposition}\label{fonctions_affines:prop:proportionnalita_accroissements}
	Soit $f$ une fonction affine.\\
	Alors, pour tous réels $u$ et $v$, $f(v) - f(v) = a(v - u)$.
\end{proposition}
\begin{proof}
	Soient $a$ et $b$ deux nombres réels, tels que $f \colon x \mapsto a×x + b$.
	\begin{align*}
		f(v) - f(u) & = a×v + b - (a×u + b) \\
								& = a×v - a×u + b - b \\
								& = a×(v - u)
	\end{align*}
\end{proof}

\subsection{Représentation graphique}

\begin{proposition}
	La représentation graphique d'une fonction affine est une droite.
\end{proposition}
\begin{proof}
	Provisoirement admis, la démonstration sera faite dans \ref{vecteurs:prop:affine_alignement}.
\end{proof}

\begin{savoirfaire}[Représenter une fonction affine]
	\begin{multicols}{2}
		Soit la fonction affine \textcolor{red}{$f(x) = -3x + 2$}.\\
		On place d'abord un premier point, celui d'abscisse $0$. Pour ce point, l'ordonnée vaut $f(0) = 2$.\\
		Comme il y a proportionnalité des accroissements (proposition~\ref{fonctions_affines:prop:proportionnalita_accroissements}), on sait que lorsque $x$ augmente de $1$, $f(x)$ varie de $-3×1$. L'ordonnée devient donc $2 - 3 = -1$. On peut donc placer le point de coordonnées $(1;-1)$.
		
		\noindent Soit la fonction affine \textcolor{blue}{$g \colon x \mapsto \frac12 x - 3$}.\\
		On place d'abord un premier point, celui d'abscisse $0$. Pour ce point, l'ordonnée vaut $g(0) = -3$.\\
		Comme il y a proportionnalité des accroissements (proposition~\ref{fonctions_affines:prop:proportionnalita_accroissements}), on sait que lorsque $x$ augmente de $2$, $f(x)$ varie de $\frac12×2 1$. L'ordonnée devient $-3 + 1 = -2$. On peut donc placer le point de coordonnées $(2;-2)$.
	\end{multicols}
	\begin{center}
		\begin{tikzpicture}[>=latex]
			\draw [blue!25] (-6,-6) grid (6,6) ;
			\draw [thick,->] (-6,0) -- (6,0) ;
			\draw [thick,->] (0,-6) -- (0,6) ;
			\foreach \i in {1,...,5} { 
				\draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; 
				\draw (-\i,0.1) -- (-\i,-0.1) node [below] {$-\i$} ;
			}
			\foreach \i in {1,...,5}  { 
				\draw (0.1,\i) -- (-0.1,\i) node [left] 	{$\i$} ; 
				\draw (0.1,-\i) -- (-0.1,-\i) node [left] {$-\i$} ;
			}
			\draw [very thick, red] plot [domain=-1.33:2.66] (\x,-3*\x + 2) ;
			\draw [very thick, blue] plot [domain=-6:6] (\x,0.5*\x - 3) ;
			
			\draw [red] (0,2) node [circle,fill,inner sep=1.5pt] {} ;
			\draw [red] (0,2) node [above right] { $(0;2)$ } ;
			\draw [red] (1,-1) node [circle,fill,inner sep=1.5pt] {} ;
			\draw [red] (1,-1) node [above right] { $(1;-1)$ } ;

			\draw [blue] (0,-3) node [circle,fill,inner sep=1.5pt] {} ;
			\draw [blue] (0,-3) node [below right] { $(0;-3)$ } ;
			\draw [blue] (2,-2) node [circle,fill,inner sep=1.5pt] {} ;
			\draw [blue] (2,-2) node [below right] { $(2;-2)$ } ;

		\end{tikzpicture}
	\end{center}
\end{savoirfaire}

\begin{savoirfaire}[Identifier une fonction affine]
	Pour identifier une fonction affine, on doit trouver son ordonnée à l'origine et son coefficient directeur.
	\begin{center}
		\begin{tikzpicture}[>=latex]
		\draw [blue!25] (-6,-6) grid (6,6) ;
		\draw [thick,->] (-6,0) -- (6,0) ;
		\draw [thick,->] (0,-6) -- (0,6) ;
		\foreach \i in {1,...,5} { 
			\draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; 
			\draw (-\i,0.1) -- (-\i,-0.1) node [below] {$-\i$} ;
		}
		\foreach \i in {1,...,5}  { 
			\draw (0.1,\i) -- (-0.1,\i) node [left] 	{$\i$} ; 
			\draw (0.1,-\i) -- (-0.1,-\i) node [left] {$-\i$} ;
		}
		\draw [very thick, red] plot [domain=-6:6] (\x,-0.3333*\x + 3) ;
		
		\draw [red] (0,3) node [circle,fill,inner sep=1.5pt] {} ;
		\draw [thick,->] (2,4) node [anchor=south west] { Ordonnée à l'origine  $b = 3$ } -- (0,3) ;
		
		\draw [thick,dashed,->] (-6,5) -- (-3,5) ;
		\draw (-4.5,5) node [above] {$\Delta x = 3$} ;
		\draw [thick,dashed,->] (-3,5) -- (-3,4) ;
		\draw (-3,4.5) node [right] {$\Delta y = -1$ } ;
		\end{tikzpicture}
	\end{center}
	Le coefficient directeur est $a = \frac{\Delta y}{\Delta x} = \frac{-1}{3}$ et l'ordonnée à l'origine $b = 3$. L'expression de la fonction est donc $y = -\frac13 x + 3$.
\end{savoirfaire}

\subsection{Variations}

\begin{proposition}\label{fonctions_affines:prop:variations}
	Soit $f \colon x \mapsto ax + b$ une fonction affine.
	\begin{itemize}
		\item Si $a>0$, alors $f$ est croissante sur $\R$.
		\item Si $a<0$, alors $f$ est décroissante sur $\R$.
		\item Si $a=0$, alors $f$ est constante sur $\R$.
	\end{itemize}
\end{proposition}
\begin{proof}
	Si $a > 0$, alors la proposition~\ref{fonctions_affines:prop:proportionnalita_accroissements} indique que $f(v) - f(u) = a(v-u)$. Si $u < v$, alors $v - u > 0$ et comme $a >0$, alors $f(v) - f(u) > 0$ et donc $f(v) > f(u)$. La fonction est donc croissante car $u < v \implies f(u) < f(v)$.\\
	Si $a < 0$, on obtient \emph{in fine} $f(v) - f(u) < 0$ car le produit de deux nombres de signe opposé est négatif. On a donc $f(v) < f(u)$. La fonction est donc décroissante car $u < v \implies f(u) > f(v)$.\\
	Si $a = 0$, alors $f(u) - f(v) = 0 \iff f(u) = f(v)$ et donc la fonction est constante.
\end{proof}

\begin{savoirrediger}[Implication $\implies$]
	Une implication, notée $A \implies B$, est un type de raisonnement mathématique. Dire que $A \implies B$ signifie que si $A$ est vraie, alors $B$ est vraie aussi.\\
	Attention, si $A$ est fausse, on ne peut rien dire sur $B$. Pour cela, il faudrait disposer d'une équivalence $\iff$.
\end{savoirrediger}

\begin{savoirfaire}[Dresser le tableau de variation d'une fonction affine]
	Dans tous les cas, il faut résoudre l'équation $f(x) = 0$.
	\begin{multicols}{2}
		\noindent $f(x) = -3x + 2$\\
		$f(x) = 0 \iff -3x + 2 = 0 \iff -3x = -2 \iff 3x = 2 \iff x = \frac23$.\\
		Ici $a = -3 < 0$, donc la fonction est décroissante.
		\begin{center}
			\begin{tikzpicture}
				\tkzTabInit[deltacl=1,lgt=2,espcl=5]{$x$/1,$f$/3}{$-\infty$,$+\infty$}
				\tkzTabVar{+/$+\infty$,-/$-\infty$}
				\tkzTabVal{1}{2}{0.5}{$\frac23$}{0}
			\end{tikzpicture}
		\end{center}
	
		\noindent $g \colon x \mapsto \frac12 x - 3$\\
		$g(x) = 0 \iff \frac12 x - 3 = \iff \frac12 x = 3 \iff x = 3×2  = 6$.\\
		Ici, $a > 0$, donc la fonction est croissante.
		\begin{center}
			\begin{tikzpicture}
				\tkzTabInit[deltacl=1,lgt=2,espcl=5]{$x$/1,$f$/3}{$-\infty$,$+\infty$}
				\tkzTabVar{-/$-\infty$,+/$+\infty$}
				\tkzTabVal{1}{2}{0.5}{$6$}{0}
			\end{tikzpicture}
		\end{center}
	\end{multicols}
\end{savoirfaire}

\section{Résolution de problèmes affines}

\subsection{Résolution d'équation}

\begin{proposition}
	\leavevmode
	\begin{itemize}
		\item L'équation $f(x) = k$ possède une unique solution, pour tout $k \in \R$.
		\item L'équation $f(x) = g(x)$ possède une unique solution si les coefficients directeurs sont différents.\\
		La solution correspond au point d'intersection des deux droites.
	\end{itemize}
\end{proposition}
\begin{proof}
	Soit $f$ une fonction affine, d'expression $f(x) = a x + b$. Résoudre l'équation $f(x) = k$ est équivalent à résoudre $g(x) = 0$, en posant $g(x) = f(x) - k$.\\
	Soient $f$ et $g$ deux fonctions affines, d'expression $f(x) = ax + b$ et $g(x) = mx + p$.\\
	Si $a ≠ m$, alors $f(x) = g(x) \iff ax + b = mx + p \iff (a - m)x = p - b$ et donc $x = \frac{p-b}{a-m}$.\\
	Si $a = m$, alors $f(x) = g(x) \iff 0x = p - b$. Si $b ≠ p$, alors l'équation ne possède aucune solution, sinon, les droites sont confondues.
\end{proof}

\subsection{Tableau de signe et résolution d'inéquations}

\begin{savoirfaire}[Dresser le tableau de signes d'une fonction affine]
	Dans tous les cas, il faut résoudre l'équation $f(x) = 0$.
	\begin{multicols}{2}
		\noindent $f(x) = -3x + 2$\\
		$f(x) = 0 \iff -3x + 2 = 0 \iff -3x = -2 \iff 3x = 2 \iff x = \frac23$.\\
		Ici $a = -3 < 0$.
		\begin{center}
			\begin{tikzpicture}
			\tkzTabInit[deltacl=1,lgt=1,espcl=3]{$x$/1,$f$/3}{,$\frac23$,}
			\tkzTabLine{+,,z,,-}
			\end{tikzpicture}
		\end{center}
		
		\noindent $g \colon x \mapsto \frac12 x - 3$\\
		$g(x) = 0 \iff \frac12 x - 3 = \iff \frac12 x = 3 \iff x = 3×2  = 6$.\\
		Ici, $a > 0$.
		\begin{center}
			\begin{tikzpicture}
			\tkzTabInit[deltacl=1,lgt=1,espcl=3]{$x$/1,$f$/3}{,6,}
			\tkzTabLine{-,,z,,+}
			\end{tikzpicture}
		\end{center}
	\end{multicols}
\end{savoirfaire}

\begin{remarque}
	Les solutions des inéquations $f(x) > 0$ et $f(x) < 0$ sont données par les tableaux de signes.
\end{remarque}

\begin{savoirrediger}[Résoudre une inéquation du premier degré]
	Pour résoudre $f(x) < k$ ou $f(x) > g(x)$, on préfère étudier $f(x) - k < 0$ ou $f(x) - g(x) > 0$. 
\end{savoirrediger}