\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}

\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Échantillonage}
\author{J.B.S. 2\up{nde}}
\date{}

\begin{document}

\maketitle

\section*{Introduction}

%\marginnote{Insérer exemples ici}

\section{Fluctuation d'échantillonage}

On considère plusieurs échantillons de taille $n$ constitué des
résultats de $n$ répétitions indépendantes d'une même expérience
aléatoire. Il est assez facile (cf. expérience de lancer de dé ou de
pièce en classe) de vérifier que ces échantillons ne donnent pas tous le
même résultat et que certains présentent une grande variation de
fréquence avec les résultat attendu.

\begin{definition}[Fluctuation d'échantillonnage]
  On appelle fluctuation d'échantillonnage la variation de fréquence
  d'une probabilité mesurée dans plusieurs répétition d'une même
  expérience aléatoire.
\end{definition}

On note :
\begin{itemize}
  \item $n$ l'effectif (ou taille) de l'échantillon ;
  \item $f$ la fréquence observée dans l'échantillon ;
  \item $p$ la proportion dans la population totale ; $p$ s'apparente à
    une probabilité.
\end{itemize}

\begin{remarque} Plus la taille de l'échantillon est grande, plus les
  fréquences observées se rapprochent de $p$.
\end{remarque}

\section{Intervalle de fluctuation}

On étudie une population donnée dont la proportion $p$ est connue (ou
supposée).

On prélève, aléatoirement, un échantillon de taille $n$ dans cette
population et on observe la fréquence $f$ du caractère étudié.

L'intervalle de fluctuation au seuil de 95\% est l'intervalle centré en
$p$ qui contient $f$ dans un échantillon de taille $n$ avec une
probabilité de \np{0,95}.

Mathématiquement, on peut écrire $\exists \alpha\in]1-p;p[,
\ \mathcal{P}(f\in[p-\alpha ; p+\alpha]) = 0,95$. Cet intervalle
$[p-\alpha ; p+\alpha]$ est l'intervalle de fluctuation de $f$.

\begin{remarque}~

  \begin{itemize}
    \item Il n'existe pas d'intervalle dans lequel on trouverait $f$
      avec certitude, à cause de la fluctuation d'échantillonage.
    \item Des simutions permettent de connaître cet intervalle.
  \end{itemize}
\end{remarque}

\begin{propriete}
  Si $0,2\leq p \leq 0,8$ et $n>25$, où $p$ est la proportion dans la
  population totale, $f$ est la fréquence d'apparition dans un
  échantillon de taille $n$, alors $\alpha = \frac1{\sqrt{n}}$

  On a donc $\mathcal{P}(f\in[p-\frac1{\sqrt{n}} ; p+\frac1{\sqrt{n}}])
  = 0,95$
\end{propriete}

\begin{remarque}
  La taille de l'intervalle de fluctuation
  $\left(\frac1{\sqrt{n}}\right)$ diminue si $n$ augmente.
\end{remarque}

On peut se servir de cet «outil» mathématique pour \emph{prendre une
décision} :
\begin{itemize}
  \item on émet une hypothèse sur la proportion $p$ ;
  \item on détermine l'intervalle de fluctuation au seuil de 95\% 
    dans des échantillons de taille $n$ ;
  \begin{itemize}
    \item si $f$ n'appartient pas à cet intervalle, on rejette
      l'hypothèse faite sur $p$ \emph{avec un risque d'erreur de 5\%};
      \item si $f$ appartient à cet intervalle, on ne rejette pas
        l'hypothèse faite sur $p$.
    \end{itemize}
\end{itemize}

\section{Intervalle de confiance}

Cette fois, on ne connaît pas la proportion $p$ dans l'intervalle de
confiance.

L'intervalle de confiance au seuil de 95\% permet d'avoir  un intervalle
où se situe la proportion inconnue $p$ avec une probabilité de 0,95.

\begin{propriete}
  On considère un échantillon de taille $n$ ($n>25$) et une fréquence
  $f$, telle que $f\in[0,2;0,8]$.

  $\mathcal{P}(p\in[f-\frac1{\sqrt{n}} ; f+\frac1{\sqrt{n}}]) = 0,95$
\end{propriete}

\begin{definition}
  D'après la propriété précédente, on a la notion d'intervalle de
  confiance à 95\% : c'est l'intervalle centré autour de $f_0$ où se
  situe la proportion $p$ avec une probabilité de 0,95. Il s'écrit
  $[f_0-\frac1{\sqrt{n}}~;~f_0+\frac1{\sqrt{n}}]$
\end{definition}

On peut se servir de cet «outil» pour estimer une proportion, un peu à
la façon des sondages :
\begin{itemize}
  \item on réalise un échantillon de taille $n$ et on y mesure la
    fréquence observée $f$ ;
  \item on construit l'intervalle de confiance à partir de $f$ et $n$.
\end{itemize}
La proportion sur la population se situe dans cet intervalle avec une
probabilité de 0,95.

Attention : il faut faire l'hypothèse d'équi-répartition du caractère
étudié.

\end{document}
