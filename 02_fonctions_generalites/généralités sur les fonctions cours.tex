\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \hfill \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbf{R}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother


\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}

\newtheorem{preuve}{Preuve}



% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{teal}{}}




\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

% colonnes
\usepackage{multicol}
\setlength{\columnseprule}{0.55pt}
\setlength{\columnsep}{30pt}

\everymath{\displaystyle\everymath{}}

\title{Généralités sur les fonctions}
\author{2\up{nd}}
\date{}

\begin{document}


\maketitle\\

\section{Définition}
Définir une \textcolor{red}{\textbf{fonction}} sur une partie D de $\R$ , c’est associer à tout nombre de D, un nombre et un seul.
On note : \\
$f : x\mapsto f(x)$ \hfill on lit :  $f$ est la fonction qui à $x$ associe $f$ de $x$ \\
$x\mapsto y $ ou $f(x)=y$\\

On dit que D est l’ensemble de définition de $f$, ou encore que $f$ est définie sur D.\\
$x$ est la variable. Le nombre $f(x)$ s’appelle l’image de $x$ par la fonction $f$.\\
Si $f(x)=y$, on dit que $x$ est un antécédent de $y$ par $f$.\\

\textbf{Exemples :} \\
$f(6) = 90$\\
90 est l’image de 6 par $f$.\\
6 a pour image 90 par $f$.\\
6 est un antécédent de 90 par $f$.\\
90 a pour antécédent 6 par $f$.\\

\section{Repère dans le plan}
Un repère du plan est formé par trois points distincts. L’origine est O et les unités sont OI et OJ.\\
Un repère est orthogonal lorsque les axes sont perpendiculaires.\\
Un repère est orthonormé ou orthonormal lorsque les axes sont perpendiculaires et OI=OJ.\\
  
\section{Courbe représentative}
\subsection{Définition}
$f$ est une fonction définie sur D.\\
Dans un repère, la courbe représentative $C$ de la fonction $f$, est l’ensemble des points de coordonnées $(x ; y)$ telles que : $x\in $D et $y = f(x)$. On dit que la courbe $C$ a pour équation $y = f(x)$ dans ce repère.\\

\textit{Remarque :} Dire qu’un point M de coordonnées $(a ; b)$ appartient à $C$ revient à dire que :
\begin{center}
$a$ est dans D et $f(a) = b$.
\end{center}

\subsection{Lectures graphiques}
\paragraph{Recherche d'image}


$f$ est une fonction définie sur D, $C$ est la représentation graphique de $f$, a est un
élément de D.
Si A est le point de $C$ d’abscisse $a$, alors $f(a)$ est l’ordonnée de A.

\textbf{Exemple : }\\
La courbe $C$ ci-contre est la représentation graphique d’une fonction $f$ définie sur $[-2 ; 2]$.\\
Pour lire graphiquement l’image de $-1,5$ c’est à dire $f(-1,5)$, on peut procéder ainsi :
\begin{itemize}
\item on repère $-1,5$ sur l’axe des abscisses et on trace, par ce point, la parallèle à l’axe
des ordonnées ;
\item  cette droite rencontre $C$ en A ;
\item on cherche ensuite l’ordonnée de A en traçant par ce point la parallèle à l’axe des
abscisses.\\
On obtient $f(-1,5) = - 1$
\end{itemize}

\begin{center}
\begin{tikzpicture}
\draw[dashed,thick,gray](-2,-5)grid(2,2);
\draw[ very thick,-latex] (-2,0) -- (2,0) node [above]{$x$};
\draw[very thick, -latex] (0,-5) -- (0,2.2) node[right]{$y$};
\draw[red, thick] (-2,1) -- (2,1);
\draw[green, thick] (-1.5,-5) - - (-1.5,2);
\draw [ thick, blue] plot [smooth] coordinates{(-2,-5)  (-1.5,-1) (-1,1) (-.75,1.75) (0,1.5) (0.75,1.75) (1,1) (2,-1.5)};
\end{tikzpicture}
\end{center}

\paragraph{Recherche d'antécédents}


$f$ est une fonction définie sur D, $C$ est la représentation graphique de $f$, b est un réel.\\
d est la droite parallèle à l’axe des abscisses passant par le point $(0 ; b)$.\\

\underline{1er cas : }\\
d ne rencontre pas $C$ : cela signifie que b n’a pas d’antécédent par $f$ dans D (ou aucun élément de D n’a b pour image par f).\\
\underline{2 ème cas : }\\
d rencontre $C$ en $A(a ; b)$. Alors A est sur $C$ donc $f(a) = b$ et a est un antécédent de b par $f$.\\

\textbf{Exemple : }\\
Reprenons la fonction précédente. Pour lire graphiquement les antécédents de 1 par $f$ :
\begin{itemize}
\item on repère 1 sur l’axe des ordonnées et on trace la droite d d’équation $y = 1$ ;
\item elle rencontre C en B et D dont les abscisses sont respectivement $-1$ et 1\\
\end{itemize}
Conclusion : $-1$ et 1 sont les antécédents de 1.

\textit{Remarque : }La lecture graphique ne donne en général qu’une valeur approchée du résultat cherché.
\section{Sens de variation d'une fonction}
\subsection{Fonction croissante, décroissante sur un intervalle}
$f $ est une fonction définie sur un intervalle I.
\begin{multicols}{2}
Dire que $f$ est \textcolor{red}{croissante} sur I signifie que pour
tous réels a et b de I :\\
si $a\leq b$ alors $f(a)\leq f(b)$.

Dire que $f$ est \textcolor{red}{décroissante} sur I signifie que pour
tous réels a et b de I :\\
si $a\leq b$ alors $f(a)\geq f(b)$.
\end{multicols}

\textbf{Exemples : }\\
$f$ est croissante sur un intervalle se traduit graphiquement par $\ll$ la courbe monte $\gg$\\
$g$ est croissante sur un intervalle se traduit graphiquement par $\ll$ la courbe descend $\gg$

\begin{multicols}{2}
\begin{tikzpicture}
\draw[ very thick,-latex] (-2.2,0) -- (4.5,0) node [above]{$x$};
\draw[very thick, -latex] (0,-1.5) -- (0,3.2) node[right]{$y$};
\draw [domain=-2.2:4.5, blue] plot (\x, {1-2/((\x)+3)}) node {$f$};
\end{tikzpicture}

\begin{tikzpicture}
\draw[ very thick,-latex] (-2.2,0) -- (4.5,0) node [above]{$x$};
\draw[very thick, -latex] (0,-1.5) -- (0,3.2) node[right]{$y$};
\draw [domain=-1:4.5, blue] plot (\x, {1+1/exp(\x)}) node {$g$};
\end{tikzpicture}
\end{multicols}
\subsection{étude du sens de variation d'une fonction}

\begin{multicols}{2}
Fonction définie sur $[-5;3]$ par sa courbe :
\begin{tikzpicture}
\draw[ very thick,-latex] (-5.2,0) -- (3.2,0) node [above left]{$x$};
\draw[very thick, -latex] (0,-1.5) -- (0,4.2) node[right]{$y$};
\draw [ thick, blue] plot [smooth] coordinates{(-5,4) (-4,-1) (-3,0) (-1,3) (0,2.75) (2,0) (3,-1)};
\draw[gray, dashed] (-5,4)- - (-5,0) node [above] {-5} ;
\draw[gray, dashed] (0,-1)-|(-4,0) node [above] {-4} ;
\draw[gray, dashed] (0,3)-|(-1,0) node [above right] {-1} ;
\draw[gray, dashed] (0,-1)-|(3,0) node [below left]  {3} ;
\end{tikzpicture}
Sens de variation de $f$:\\
\begin{itemize}
\item La fonction $f$ est décroissante sur $[-5;-4]$;
\item La fonction $f$ est croissante sur $[-4;-1]$;
\item La fonction $f$ est décroissante sur $[-1;3]$;
\end{itemize}

\end{multicols}
tableau de variation : \\
\begin{center}
 \begin{tikzpicture}
      \tkzTabInit[espcl=1.5]
      {$x$ / 1   ,$f$/2}%
      {$-5$, $-4$, $-1$, $3$ }%
      \tkzTabVar{+/$4$,-/$-1$,+/$3$,-/$-1$}
    \end{tikzpicture}
\end{center}
    
    \subsection{Maximum et minimum d'une fonction}
$f$ est une fonction, I un intervalle inclus dans son domaine de définition et a un réel de I.\\
\begin{itemize}
\item Dire que $f(a)$ est le minimum de $f$ sur I signifie que $f(a)$ est la plus petite valeur de la fonction : \\
pour tout réel $x$ de I, $f(x) \geq f(a)$.
\item Dire que $f(a)$ est le maximum de $f$ sur I signifie que $f(a)$ est la plus grande valeur de la fonction :\\
pour tout réel $x $de I, $f(x) \leq f(a)$. 
\end{itemize} 

\textbf{Exemple : }\\
Le minimum sur l’intervalle $[-5 ; 6]$ de la fonction $f$ représentée $-2$.\\
 Il est obtenu lorsque $x =\dfrac{3}{2}$ . 
 En effet, A est le point le plus « bas » de la courbe.\\
Le maximum sur l’intervalle $[-5 ; 6]$ de la fonction $f$ représentée est 4. \\
Il est obtenu lorsque $x = -3$. 
En effet, B est le point le plus « haut » de la courbe.\\

\begin{center}
\begin{tikzpicture}
\draw[dotted,thick,gray](-5,-2)grid(7,5);
\draw[ very thick,-latex] (-5.2,0) -- (6.4,0) node [above left]{$x$};
\draw[very thick, -latex] (0,-2.5) -- (0,4.2) node[right]{$y$};
\draw [ thick, blue] plot [smooth] coordinates{(-5,2) (-3,4) (1.5,-2) (6,3)};
\draw[red, dashed, very thick] (-3,4)- - (0,4) node[right]{4} ;
\draw[red, dashed, very thick] (1.5,-2)- - (0,-2) node[left]{$-2$} ;
\draw[red, dashed, very thick] (-3,4)- - (-3,0) node[below]{$-3$} ;
\draw[red, dashed, very thick] (1.5,-2)- - (1.5,0) node[above]{$1,5$} ;
\end{tikzpicture}
\end{center}
\newpage
\begin{center}
{\LARGE \textsc{Compétences}}\\
\end{center}

\begin{itemize}
\item Traduire le lien entre deux quantités par une formules
\item Déterminer l'ensemble de définition d'une fonction
\item Calculer une image par une fonction (en utilisant la forme la plus adaptée notamment)
\item Tracer la courbe représentative d’une fonction
\item Déterminer si un point appartient à la courbe représentative d’une fonction ou non
\item Déterminer graphiquement l’image d’un réel par une fonction
\item Déterminer graphiquement le ou les antécédent(s) d’un réel par une fonction
\item Utiliser la calculatrice pour obtenir un tableau de valeurs et une courbe
\item Résoudre graphiquement une équation
\item Résoudre graphiquement une inéquation
\item Construire le tableau de signes d’une fonction
\item Construire le tableau de variations d’une fonction
\item Déterminer graphiquement le minimum ou le maximum d’une fonction
\end{itemize}
\end{document}