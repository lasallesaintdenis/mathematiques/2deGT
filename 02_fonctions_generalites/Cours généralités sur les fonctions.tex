\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}
\newtheorem{exemple}{Exemple}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Généralité sur les fonctions}
\author{J.B.S. 2\up{nde}}
\date{}

\begin{document}

\vfill

\maketitle

\vfill
\pagebreak

\section{Caractérisation des nombres}

\subsection{Le début de la caractérisation de la notion de nombre}

Une première théorie «naïve» permet de regrouper des «objets» possédant
une propriété commune de façon à former un ensemble. Parmi ces «objets»,
considérons l'ensemble des nombres -- c'est à dire des «objets» qui ont la
propriété d'être un nombre. Les «objets» consitutifs d'un ensemble en
sont les \emph{éléments}. Un ensemble se décrit, soit en énumérant tous
ses éléments, on dit alors en \emph{extension} ; soit en donnant la
propriété commune, on dit alors en \emph{compréhension}.

\begin{exemple}
  L'ensemble $\{0 ; 1 ; 2 ; 3 ; 4\}$ est un ensemble à 5 éléments.
\end{exemple}

Plus généralement, l'ensemble précédent est un \emph{sous-ensemble} de
l'ensemble des nombres naturels, ou nombre entiers, qu'on note
$\mathbf{N}$ dans les documents imprimés et $\mathbb{N}$ en écriture
scripturale.

L'ensemble des nombres naturels est le premier ensemble qu'on peut
définir facilement. Il n'a pas de plus grand élément (une façon de dire
qu'il est infini), et son plus petit élément. De plus, un sous-ensemble
non vide majoré de cet ensemble est finie.

On qualifie l'ensemble $\mathbf{N}$ de dénombrable.

\subsection{Les opérations naturelles et leurs réciproques}

Comme on sait le faire depuis quelques années, l'addition et la
multiplication sont des oérations naturelles dans $\mathbf{N}$.

Soient deux nombres entiers naturels $a$ et $b$. On cherche à «inverser»
l'opération de multiplication par $b$ Autrement dit, on cherche un
nombre $q$ tel que, par exemple $b\times q = a$. Lorsque ce nombre
existe, on dit que $b$ divise $a$. Sinon, on dit qu'il ne divise pas
$a$. Dans ce deuxième cas, on peut trouver $q$ et $r$ de façon unique,
en imposant $0<r<b$ et $a = bq + r$. On parle de division
\emph{euclidienne}.

Voyons désormais ce qui se passe dans le cas de la soustraction. On
garde le nom de $a$ et $b$ pour désigner deux nomnbres entiers naturels.
Si $a\leq b$, l'opération $b - a$ possède bien un sens dans l'ensemble
de nombres entiers naturels. Sinon, on ne dispose pas de nombres pour
représenter la quantité $b - a$ dans le cas où $a>b$.

Pour pallier à cette «insuffisance», on complète l'ensemble des entiers
naturels avec les opposés de tous les nombres, sauf zéro, qui est son
propre opposé. L'ensemble ainsi obtenu est l'ensemble des nombres
entiers relatifs, noté $\mathbb{Z}$ ou $\mathbf{Z}$.

Arrêtons nous un peu sur les notations. Lorsqu'un point $M$ appartient à
une droite $\mathscr{D}$, on note $M\in\mathscr{D}$. De la même façon
désormais, lorsqu'un élément $x$ appartient à un ensemble $E$, on note
$x\in E$. Dire que $F$ est un sous ensemble de $E$, c'est dire que tous
les éléments de $F$ sont aussi des éléments de $E$. On le note $F\subset
E$. On parle de l'\emph{inclusion} et on dit que $F$ est \emph{inclus}
dans $E$. Si tous les éléments de $E$ sont des éléments de $F$ et si
tous les éléments de $F$ sont des éléments de $E$, alors on a la double
inclusion $E\subset F$ et $F\subset E$ et donc $E=F$.

Revenons à la réciproque de nos opérations. On a pu étendre l'ensemble
des entiers naturels en formant l'ensemble des entiers relatifs de telle
façon que $\mathbf{N}\subset\mathbf{Z}$ et que la soustraction possède
toujours une solution unique. La propriété de divisibilité euclidienne,
c'est à dire l'existence d'un unique couple de nombres $(q ; r)$ tels
que pour tous nombres entiers naturels $a$ et $b$, $a = bq + r$, avec
$0\leq r < b$ reste vraie parmi les nombres entiers relatifs. Mais cette
forme ne se prête pas toujours bien aux calculs.

\subsection{Deux améliorations possibles}

Une première idée est de chercher à réellement inverser l'opération de
multiplication pour tous les nombres différents de 0. On cherche en fait
à résoudre l'équation $ax = b$, où $a$ et $b$ sont deux nombres entiers
différents de 0. On va utiliser une astuce : notons $a$ sous la forme
$(a ; 1)$, $x$ sous la forme $(x; 1)$ et $b$ sous la forme $(b ; 1)$.
$a\times x$ devient $(a\times x ; 1\times 1)$. L'égalité de deux de ces
couples particuliers peut s'écrire de la façon suivante : $(p ;q) = (r
;s)$ si $ps = qr$. Avec cette convention, on vérifie que $(x;1) =
(b;a)$.
\begin{exercice}
  Vérifier qu'avec cette égalité de couple, $(p,q) + (r,s) = (ps + qr,
  qs)$
\end{exercice}
Cette notation n'étant pas aisée à manipuler, on lui préferera la
notation $\frac{a}{b}$ où $b\neq0$. Et lorsque $b=10 ; 100 ; 1000 ; …$
on utilisera $0,a ; 0,0a ; 0,00a ; …$ On parle alors de nombre décimaux.

Cet ensemble ainsi construit s'appelle le corps de fractions ou
l'ensemble des quotients entiers. On le note $\mathbf{Q}$ $\mathbb{Q}$.

Une seconde idée est d'admettre l'existence de nombres décimaux et
réiter le processus de division par $0,1b$, puis $0,01b$, etc jusqu'à
obtenir une approximation «unique» du résultat. On construit ainsi deux
nouveaux ensembles de nombres.

Considérons l'ensemble $0,1\mathbf{Z} = \{ 0,1\times x \mid x\in\mathbf{Z}
\} \not\subset \mathbf{Z}$. En fait, on a que $\mathbf{Z} \in
0.1\mathbf{Z}$. Si on ne s'intéresse qu'aux 20 nombres positifs de cet
ensemble, on obtient le sous ensemble $\{ 0 ; 0,1 ; 0,2 ; \dots ; 0.9 ;
1 ; 1,1 ; 1,2 \dots ; 1,9 \}$. Ce sont les nombres décimaux limités au
dixième. On peut tenir le même raisonnement avec de plus en plus de
décimales et obtient au final l'ensemble des nombres décimaux, noté
$\mathbf{D}$ ou $\mathbb{D}$.

\subsection{Les fractions ne permettent pas encore de représenter tous
les nombres}

Ce fait est connu depuis longtemps, mais il existe certains nombres
qu'on ne peut représenter sous forme de fractions, comme $\sqrt{2}$ ou
$\pi$. Cependant, on peut trouver, pour ces deux nombres des fractions
ou des développement décimaux qui approchent aussi près que l'on veut
ces nombres. Et ces nombres ont une existence, puisque le premier
correspond à la longueur de la diagonale du carré et le second à la
longueur du demi-cercle de rayon unité -- ou la longueur d'un cercle de
diamètre 1.

On va donc admettre l'existence de ces nombres là et plus généralement
d'un ensemble de nombres situés entre deux nombres rationnels. Tant
quà faire, on va postuler qu'entre deux nombres rationnels, il existent
une infinité de nombres. Ce nouvel ensemble de nombre s'appelle le corps
des réels. On le note $\mathbf{R}$ ou $\mathbb{R}$.

On a désormais les inclusions suivantes : \[ \mathbf{N} \subset
\mathbf{Z} \subset \mathbf{Q} \subset \mathbf{D} \subset \mathbf{R} \]

L'ensemble des nombres réels étant vaste, on se retreint souvent à un de
ses sous ensembles, caractérisé par l'ordre. Soient $a$ et $b$ deux
nombres réels, tels que $a<b$ et soit l'ensemble $I = \{ x\in \mathbf{R}
\mid a\leq x \leq b\}$ des nombres réels compris entre $a$ et $b$. On
appelle un sous ensemble de $\mathbf{R}$ un intervalle fermé et on note
$I = [a ; b]$.

On distingue ainsi 8 types d'intervalles ouverts qui complètent l'unique
forme d'intervalle fermé.
\begin{exercice}
  Associe les intervalles avec leur définition
  \begin{multicols}{2}
    \begin{enumerate}
      \item $[a; b [$
      \item $]a; b [$
      \item $]a; b ]$
      \item $[a; +\infty [$
      \item $]a; +\infty [$
      \item $]-\infty ; b ]$
      \item $]-\infty ; b [$
      \item $]-\infty ; +\infty[ $
    \end{enumerate}
    \begin{enumerate}
      \item $\{ x \in \mathbf{R} \mid a < x \}$
      \item $\{ x \in \mathbf{R} \mid a < x < b \}$
      \item $\{ x \in \mathbf{R} \mid a \leq x < b \}$
      \item $\{ x \in \mathbf{R} \mid a \leq x \}$
      \item $\{ x \in \mathbf{R} \mid a < x \leq b \}$
      \item $\{ x \in \mathbf{R} \mid x \leq b \}$
      \item $\mathbf{R}$
      \item $\{ x \in \mathbf{R} \mid x < b \}$
    \end{enumerate}
  \end{multicols}
\end{exercice}

\section{Développer -- factoriser}

Jusqu'à présent, on ne s'est intéressé aux opérations
qu'individuellement ou prises avec leur opération réciproque, c'est à
dire en considérant l'addition et la soustraction, puis la
multiplication et la division. Avec les définitions de nombres que nous
possédons, on peut ne s'intéresser qu'à l'addition et la multiplication.
Étudions leur rapport entre elles et leurs interactions de l'une sur
l'autre.

Tout d'abord, la multiplication se définit sur les nombres entiers de la
façon suivantes par : pour $n$ et $p$ deux nombres entiers, \[n \times p
= \underbrace{n+n+n+\cdots+n}_{p\text{ termes}}. \]
Cette définition est ensuite étendue aux autres ensembles et possède une
propriété remarquable, vraie pour tous nombres $a$, $b$, et $c$ réels :
\[ a\times (b+c) = a\times b + a\times c .\]

Bien évidement, cette égalité fondamentale reste vraie en remplaçant la
multiplication par la division ou l'addition par la soustraction, ou
encore en ajoutant plus de termes dans la parenthèse ou en transormant
le premier facteur en une somme. Certains développement donnent même des
identités qu'il est nécessaire de remarquer :
\begin{enumerate}
  \item
  \item
  \item
\end{enumerate}

\section{Notion de fonction}

\subsection{À la découverte du concept}

On a l'habitude de dire, dans le langage de la vie courante «en fonction
de». Quelle notion mathématique recouvre cette expression ? Partons
explorer !

Pour pouvoir parler de fonction, il faut avoir des «trucs» au départ qui
vont devenir des «machins» (ou le contraire, à moins que les «trucs»
restent des «trucs».) Pour simplifier, ici, nous n'allons considérer
presque que des nombres.

De la même façon que le premier ensemble donné en exemple a été donné en
extension, on peut, dans certains cas, définir une fonction de cette
façon. Prenons par exemple $E= \{ 1; 2 ; 3; \dots; 26 \}$ et $F = \{ A ;
B ; C; \dots ; Z \}$. On peut ici définir la fonction $f$ telle que
l'\emph{image} par $f$ de 1 soit $A$, etc, jusqu'à l'image par $f$ de 26
qui est $Z$. On dit alors que $2$ est l'\emph{antécédent} de $B$ par
$f$. On dit aussi que par $f$ j'\emph{associe} 3 à $C$ et on note
$f:4\mapsto D$.

Toujours avec ces deux ensembles, je peux également construire la
fonction $g$ qui à $A$ associe 1 etc. Cette fois, l'image de $A$ par $g$
est 1 et l'antécédent de $2$ est $B$. Autrement dit, on peut retenir que
l'image est un élément de l'ensemble d'arrivée et l'antécédent est un
élement de l'ensemble de départ. La précision de ces espaces est
essentielle, on le verra par la suite.

On aura bien remarqué ici que le cas présent était fort simple : les
deux ensembles considérés $E$ et $F$ avaient exactement le même nombré
d'éléments. Considérons désormais l'ensemble $E'$ formé des entiers
naturels de 1 à 52 et considérons une nouvelle fonction $f$ de la façon
suivante : à 1, on associe $A$ jusqu'à 26 qui associe $Z$ puis à 27 on
associe $A$ jusqu'à 52 auquel on associe $Z$. Si on se demande désormais
quel est l'antécédent de $C$, on trouve 3 et 29. Vous l'aurez compris,
l'antécédent n'est plus unique. Il faut désormais parler de \emph{un}
antécédent.

Cette méthode de définition n'est pas forcément la plus pratique : il
faut donner la liste de toutes les associations, ce qui n'a pas été fait
ici, faisant appel à votre intelligence pour compléter les associations
manquantes. Lorsque les deux ensembles de départ et d'arrivés sont des
nombres, on préfère donner la fonction sous la forme d'une expression
algébrique, comme $f:\mathbf{N}\to\mathbf{N}, p\mapsto 2p+1$ pour la
fonction qui associe aux entiers naturels les nombres impairs. Ici,
$2p+1$ est l'\emph{expression algébrique} de la fonction $f$ associée à
un certain entier $p$. Si on l'avait associé à un entier $x$, on aurait
noté $f(x)=2x+1$.

\subsection{Représentation graphique d'une fonction}

Lorsqu'on a à faire à une fonction qui à des valeurs numériques associe
des valeurs numériques, il est fréquent de représenter tout ou partie de
cette fonction graphiquement.

\begin{center}
  \begin{multicols}{2}
    \begin{tikzpicture}[scale=0.7]
    \draw [->] (-0.5,0) -- (6.5,0) ;
    \draw [->] (0,-0.5) -- (0,6.5) ;
    \draw node [shape=cross] (0,3) {} ;
    \draw node [shape=cross] (1,1) {} ;
    \draw node [shape=cross] (2,2) {} ;
    \draw node [shape=cross] (3,0) {} ;
    \draw node [shape=cross] (4,3) {} ;
    \draw node [shape=cross] (5,3) {} ;
    \draw node [shape=cross] (5,5) {} ;
  \end{tikzpicture}

  Une fonction entre deux ensembles finis
  \columnbreak

  \begin{tikzpicture}[scale=0.7]
    \draw [->] (-0.5,0) -- (6.5,0) ;
    \draw [->] (0,-0.5) -- (0,6.5) ;
    \draw plot [domain=0:2.25] (\x,{\x^2}) ;
  \end{tikzpicture}

  Une fonction entre deux intervalles de $\mathbf{R}$
\end{multicols}
\end{center}

Une fonction peut-être entièrement définie par son graphe : celui-ci
donne l'ensemble de départ, l'ensemble d'arrivée et les associations
entre les éléments de l'ensemble de départ et les éléments de l'ensemble
d'arrivée.

L'ensemble de départ s'appelle aussi \emph{domaine de définition} de la
fonction. Celui-ci est d'ailleurs la première choses nécessaire pour
tracer le graphe (ou courbe représentative) d'une fonction à partir de
son expression, là où celle-ci possède un sens. En effet, une fonction
dont l'expression algéprique est $\frac{1}{x}$ ne possède pas de sens
lorsque $x=0$ et il faut donc exclure $x$ du domaine de définition de la
fonction, ou compléter la définition de la fonction par une valeur aux
points où l'expression n'a pas de sens. On écrira par exemple : \[
  f:]-\infty ; 0 [ \cup ]0 ; +\infty[ \to \mathbf{R} ; x\mapsto
\frac{1}{x}, \] où $\cup$ indique l'\emph{union}, c'est à dire que la
variable $x$ peut appartenir à l'ensemble de gauche et à l'ensemble de
droite. On note également parfois $\mathbf{R}\setminus\{0\} =
\mathbf{R}^*$ l'ensemble des réels privé de 0 et $\mathbf{R}_+$ (resp.
$\mathbf{R}_-$) l'ensemble des nombres réels positifs, 0 inclus (resp.
l'ensemble des nombres négatifs, 0 inclus.)

Cette représentation graphique sert en particulier à résoudre
graphiquement des équations du type $f(x) = g(x)$ où $f(x)$ et $g(x)$
sont les expressions des fonctions $f$ et $g$. L'idée est de tracer les
deux fonctions dans le même repère et de repérer la position des points
d'intersections des deux fonctions.

\subsection{Variations d'une fonction}

Une des notions importantes relative aux fonctions est leur variation.
Graphiquement, on peut observer si la fonction croît ou décroît sur un
certain intervalle.

Mathématiquement parlant, pour une fonction $f$ d'expression $f(x)$, on
dit que $f$ est croissante (resp. décroissante) sur un intervalle $I$,
si pour tout $x\in I$ et pour tout $y\in I$, si $x\leq y$, alors $f(x)
\leq f(y)$ (resp. $f(x) \geq f(y)$.) On parle de stricte croissance (ou
décroissance) lorsque les inégalités sont srtictes.

Une fonction qui n'est ni croissante, ni décroissante sur $I$,
intervalle est constante sur cet intervalle.

Une fonction qui est croissante ou décroissante sur un intervalle est
dite \emph{monotone}.

S'il existe un point $x_0$ tel que pour tout $x<x_0$, $f(x)<f(x_0)$ et
pour tout $x>x_0$, $f(x)<f(x_0)$, on dit que $x_0$ est un maximum local
de la fonction $f$. Ainsi la fonction est d'ailleurs croissante jusqu'à
ce maximum, puis décroissante. De la même façon, on parle de minimum
local lorsqu'il existe $x_0$ tel que pour tout $x<x_0$, $f(x)>f(x_0)$ et
pour tout $x>x_0$, $f(x)>f(x_0)$.

\emph{Note :} Puisqu'on va être amené à souvent utiliser les expressions
«pour tout x appartenant à …» et «il existe x appartenant à …»,
convenons de noter la première «$\forall x \in …$» et la seconde
«$\exists x \in …$».

\end{document}
