\chapter{Fonctions de référence}

\section{Fonction carré}

\subsection{Définition de la fonction carré}

\begin{definition}
	La fonction définie sur $\R$, qui à tout $x$ associe son carré $x^2$, est appelée \textbf{fonction carré}.
\end{definition}

\subsection{Sens de variation de la fonction carré}

\begin{proposition}
  \label{fonctionref:prop:variations}
	La fonction carré est décroissante sur $\intv[o]{-\infty}{0}$ et croissante sur $\intv[o]{0}{+\infty}$. Son tableau de variation est 
	\begin{center}
		\begin{tikzpicture}
			\tkzTabInit[deltacl=1]{$x$/1,$f$/3}{$-\infty$,$0$,$+\infty$}
			\tkzTabVar{+/$+\infty$,-/$0$,+/$+\infty$}
		\end{tikzpicture}
	\end{center}
\end{proposition}
\begin{proof}
	Soient $u$ et $v$ deux nombres négatifs, tels que $u < v < 0$.\\
	$u^2 - v^2 = (u-v)(u+v)$. Comme $u < v \implies u - v < 0$ et $u < 0$ et $v < 0$, alors $u + v$ est négatif. Ainsi, $(u-v)(u+v)$ est positif et donc $u^2 - v^2 > 0 \iff u^2 > v^2$. La fonction carré est donc décroissante sur $\intv[o]{-\infty}{0}$.\\[2ex]
	Soient $u$ et $v$ deux nombres positifs, tels que $0 < u < v$.\\
	$u^2 - v^2 = (u-v)(u+v)$. Comme $u < v \implies u - v < 0$ et $u > 0$ et $v > 0$, alors $u + v$ est positif. Ainsi, $(u-v)(u+v)$ est négatif et donc $u^2 - v^2 < 0 \iff u^2 < v^2$. La fonction carré est donc croissante sur $\intv[o]{0}{+\infty}$.
\end{proof}

\begin{savoirrediger}[Raisonnement par disjonction des cas]
	Dans la démonstration ci-dessus, on doit distinguer deux cas différents, qui conduisent à des résultats différents, bien que regroupées dans une même proposition. On dit qu'un tel raisonnement est par \textbf{disjonction des cas}.
\end{savoirrediger}

Les variations de la fonction carré permettent de comparer des nombres.

\begin{exemple}
  Soit $x$ un nombre réel tel que $2 ≤ x < 4$. On a alors $4 ≤ x < 16$.
  Autrement dit, l'image de l'intervalle $\intv[r]{2}{4}$ par la fonction
  carré est $\intv[r]{4}{16}$.

  Soit $x$ un nombre réel tel que $-1 ≤ x < 0$. On a alors $0 < x ≤ 1$, car
  les nombres sont négatifs.
\end{exemple}

\begin{remarque}
  Pour comparer les carrés de nombres de signes différents, il faut procéder
  par disjonction des cas :\\
  $-2 ≤ x ≤ 4 \implies -2 ≤ x ≤ 0$ et $0 ≤ x ≤ 4 \implies 0 ≤ x ≤ 4$ et $0 ≤
  x ≤ 16 \implies 0 ≤ x ≤ 16$ car $\intv{0}{4} \subset \intv{0}{16}$.
\end{remarque}

\subsection{Représentation graphique de la fonction carré}

\begin{definition}
	La courbe représentative de la fonction carrée dans un repère orthonormé s'appelle une \textbf{parabole} de sommet $O$.
\end{definition}

\begin{note}[Tableau de valeurs de la fonction carré]
	\begin{center}
		\begin{tabular}{|l|*{7}{>{$}c<{$}|}} \hline
			$x$ & -3 & -2 & -1 & 0 & 1 & 2 & 3 \\ \hline
			$f(x)$ & 9 & 4 & 1 & 0 & 1 & 4 & 9 \\ \hline
		\end{tabular}
	\end{center}
	Le tableau n'étant pas un tableau de proportionnalité, la fonction n'est donc pas linéaire.
\end{note}

\begin{center}
	\begin{tikzpicture}[>=latex]
		\draw [blue!25] (-6,-1) grid (6,11) ;
		\draw [thick,->] (-6,0) -- (6,0) ;
		\draw [thick,->] (0,-1) -- (0,11) ;
		\foreach \i in {1,...,5} { 
			\draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; 
			\draw (-\i,0.1) -- (-\i,-0.1) node [below] {$-\i$} ;
		}
		\foreach \i in {1,...,10}  { 
			\draw (0.1,\i) -- (-0.1,\i) node [left] 	{$\i$} ; 
		}
	
		\draw [very thick,red] plot [domain=-3.31:3.31,smooth] (\x,{\x*\x}) ; 
		
		\draw (2.236,5) node [fill,circle,blue,inner sep=1pt] {};
		\draw (2.236,5) node [below right,blue] {$M: (x,x^2)$} ;
		\draw (-2.236,5) node [fill,circle,blue,inner sep=1pt] {};
		\draw (-2.236,5) node [below left,blue] {$M': (-x,x^2)$} ;
		\draw [thick,dashed,blue] (2.236,0) -- (2.236,5) -- (-2.236,5) -- (-2.236,0) ;
	\end{tikzpicture}
\end{center}

\begin{proposition}
	L'axe $[Oy)$ des ordonnées est un axe de symétrie de la parabole.
\end{proposition}
\begin{proof}
	La parabole a pour équation $y = x^2$, les points $M$ et $M'$ sont bien symétriques. Il suffit pour cela de comparer leurs coordonnées.
\end{proof}

\subsection{Résolution des équations carrés}

\begin{proposition}
  Soit $k$ un nombre réel. Notons $S$ l'ensemble des solutions de l'équation
  $x^2 = k$.
  \begin{itemize}
    \item Si $k < 0$, l'équation $x^2 = k$ ne possède pas de solutions réelles
      et on note $S = ø$.
    \item Si $k = 0$, l'équation $x^2 = k$ possède une seule solution et on
      note $S = \set{0}$.
    \item Si $k > 0$, l'équation $x^2 = k$ possède deux solutions et on note
      $S = \set{-\sqrt{k} ; \sqrt{k}}$.
  \end{itemize}
\end{proposition}
\begin{proof}
  Soit $k$ un nombre réel.
  \begin{itemize}
    \item Si $k < 0$, comme $x^2 ≥ 0$, l'équation ne possède pas de
      solution, ce qu'on représente avec l'ensemble vide $ø$.
    \item Si $k = 0$, alors on résout l'équations $x^2 = 0$ qui possède 0
      comme solution et seulement 0, puisque, pour tout $x ≠ 0, x^2 ≠ 0$.
    \item Si $k > 0$, alors $x^2 = k \iff x^2 - k = 0 \iff x^2 -
      \brk*{\sqrt{k}}^2 = 0 \iff (x - \sqrt{k})(x + \sqrt{k}) = 0$. On
      trouve donc deux solutions $\sqrt{k}$ et $-\sqrt{k}$.
  \end{itemize}
\end{proof}

\section{Fonction cube}

\subsection{Définion}

\begin{definition}
  La fonction définie sur $\R$ par $f: \mapsto x^3$ s'appelle la
  \textbf{fonction cube}.
\end{definition}

\subsection{Sens de variation de la fonction cube}

\begin{proposition}
  La fonction cube est croissante sur $\R$. Son tableau de variation est 
	\begin{center}
		\begin{tikzpicture}
			\tkzTabInit[deltacl=1]{$x$/1,$f$/3}{$-\infty$,$+\infty$}
			\tkzTabVar{-/$-\infty$,+/$+\infty$}
		\end{tikzpicture}
	\end{center}

\end{proposition}
\begin{proof}
  Soient $x$ et $y$ deux réels. On a $x^3 - y^3 = (x-y)( (x - \frac{y}2)^2 +
  \frac{3}{4}y^2)$. On en déduit que $x^3 - y^3$ est du signe de $x- y$ et donc 
  \begin{itemize}
    \item $x ≤ y \implies x - y ≤ 0 \implies x^3 - y^3 ≤ 0 \implies x^3 ≤ y^3
      $
    \item $x ≥ y \implies x - y ≥ 0 \implies x^3 - y^3 ≥ 0 \implies x^3 ≥ y^3
      $
  \end{itemize}
\end{proof}

\begin{note}[Tableau de valeurs de la fonction cube]
	\begin{center}
		\begin{tabular}{|l|*{7}{>{$}c<{$}|}} \hline
			$x$ & -3 & -2 & -1 & 0 & 1 & 2 & 3 \\ \hline
			$f(x)$ & -27 & -8 & -1 & 0 & 1 & 8 & 27 \\ \hline
		\end{tabular}
	\end{center}
	Le tableau n'étant pas un tableau de proportionnalité, la fonction n'est donc pas linéaire.
\end{note}

\begin{center}
	\begin{tikzpicture}[>=latex,yscale=0.3]
		\draw [blue!25] (-6,-30) grid (6,30) ;
		\draw [thick,->] (-6,0) -- (6,0) ;
		\draw [thick,->] (0,-30) -- (0,30) ;
		\foreach \i in {1,...,5} { 
			\draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; 
			\draw (-\i,0.1) -- (-\i,-0.1) node [below] {$-\i$} ;
		}
		\foreach \i in {1,3,...,29}  { 
			\draw (0.1,\i) -- (-0.1,\i) node [left] 	{$\i$} ; 
			\draw (0.1,-\i) -- (-0.1,-\i) node [left] 	{$-\i$} ; 
		}
	
		\draw [very thick,red] plot [domain=-3.1:3.1,smooth] (\x,{\x*\x*\x}) ; 
		
    \draw (1.71,5) node [fill,circle,blue,inner sep=1pt] {};
    \draw (1.71,5) node [below right,blue] {$M: (x,x^3)$} ;
    \draw (-1.71,-5) node [fill,circle,blue,inner sep=1pt] {};
    \draw (-1.71,-5) node [below left,blue] {$M': (-x,-x^3)$} ;
		\draw [thick,dashed,blue] (1.71,0) -- (1.71,5) -- (-1.71,-5) -- (-1.71,0) ;
	\end{tikzpicture}
\end{center}

\begin{proposition}
  L'origine $O\ (0;0)$ du repère est le centre de symétrie de la courbe.
\end{proposition}
\begin{proof}
%	La parabole a pour équation $y = x^2$, les points $M$ et $M'$ sont bien symétriques. Il suffit pour cela de comparer leurs coordonnées.
\end{proof}


\section{Fonction racine carré}

\subsection{Définition de la fonction racine carré}

Sur $\R_+$, la fonction carré est croissante et chaque antécédent possède
exactement une et une seule image. On peut odnc, à chaque image, associer
son antécédent. On crée ainsi une nouvelle fonction, appellée
\textbf{fonction réciproque} de la fonction carré ou encore \textbf{fonction
racine carré}

\begin{definition}\label{def:racine_carre}
  On appelle \textbf{fonction racine carré} la fonction, qui à tout nombre
  positif $x$ associe le nombre positif $y$ tel que $y^2 = x$.

  On le note $\sqrt{x}$
\end{definition}

\begin{remarque}
  Le carré et la racine carré sont deux fonctions réciproques sur $\R_+$.

  La racine carré s'obtient en tapant \texttt{$2nde+x^2$}
\end{remarque}

\begin{proposition}
  Pour tout nombre positif réel, $\brk{\sqrt{x}}^2 = x$
\end{proposition}
\begin{proof}
  Il faut relire la définition \ref{def:racine_carre}
\end{proof}

\begin{proposition}
  Pour tout nombre réel $x$, $\sqrt{x^2} = \abs{x}$.
\end{proposition}
\begin{proof}
  Soit $x$ un nombre réel.
  \begin{itemize}
    \item Si $x ≥ 0$, on a $\sqrt{x^2} = x = \abs{x}$
    \item Si $x < 0$, on a $\sqrt{x^2} = -x = \abs{x}$
      % TODO: À développer
  \end{itemize}
\end{proof}

On peut noter cette inégalité fondamentale, aussi connue sous le nom
d'inégalité triangulaire.

\begin{proposition}
  $\forall (a,b) \in \intv[o]{0}{+\infty},\ \sqrt{a+b} < \sqrt{a} + \sqrt{b}
  $
\end{proposition}
\begin{proof}
  On élève au carré à droite et à gauche et on raisonne par équivalences
  successives.
  % TODO: À rédiger
\end{proof}

\begin{savoirrediger}
  On peut raisonner par équivalences successives, où toutes les propositions
  sont logiquement équivalentes et donc la véracité de la première est
  garantie par la véracité de la dernière.
\end{savoirrediger}

\subsection{Sens de variation}

\begin{proposition}
  La fonction racine est croissante sur $\R_+$. Son tableau de variation est 
	\begin{center}
		\begin{tikzpicture}
			\tkzTabInit[deltacl=1]{$x$/1,$f$/3}{$0$,$+\infty$}
			\tkzTabVar{-/$0$,+/$+\infty$}
		\end{tikzpicture}
	\end{center}

\end{proposition}
\begin{proof}
  On raisonne par l'absurde. Soient $x$ et $y$ deux nombres réels positifs
  tels que $x < y$ et $\sqrt{x} > \sqrt{y}$.
  Comme la fonction carré est croissante, $\sqrt{x} > \sqrt{y} \implies x >
  y$. On aurait alors $x < y$ et $x > y$, ce qui est impossible.
\end{proof}

\begin{center}
	\begin{tikzpicture}[>=latex]
		\draw [blue!25] (-1,-1) grid (10,4) ;
		\draw [thick,->] (-1,0) -- (10,0) ;
		\draw [thick,->] (0,-1) -- (0,4) ;
		\foreach \i in {1,...,9} { 
			\draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; 
		}
		\foreach \i in {1,...,4}  { 
			\draw (0.1,\i) -- (-0.1,\i) node [left] 	{$\i$} ; 
		}
	
    \draw [very thick,red] plot [domain=0:0.9,smooth] (\x,{sqrt(\x)}) ; 
    \draw [very thick,red] plot [domain=0.9:9.9,smooth] (\x,{sqrt(\x)}) ; 
  \end{tikzpicture}
\end{center}


\section{Fonction inverse}

\subsection{Définition de la fonction inverse}

\begin{definition}
  La fonction définie sur $\R^* = \intv[o]{-\infty}{0} \cup
  \intv[o]{0}{+\infty}$ par $f(x) = \frac1x$ est appelée \textbf{fonction
  inverse}.
\end{definition}

\subsection{Sens de variation de la fonction inverse}

\begin{proposition}
	La fonction inverse est décroissante sur $\intv[o]{-\infty}{0}$ et décroissante sur $\intv[o]{0}{+\infty}$. Son tableau de variation est 
	\begin{center}
		\begin{tikzpicture}
		\tkzTabInit[deltacl=1]{$x$/1,$f$/3}{$-\infty$,$0$,$+\infty$}
		\tkzTabVar{+/0,-D+/$-\infty$/$+\infty$,-/0}
		\end{tikzpicture}
	\end{center}
\end{proposition}
\begin{proof}
	Soient $u$ et $v$ deux nombres non nuls. $\frac1u - \frac1v = \frac{v}{uv} - \frac{u}{uv} = \frac{v - u}{uv}$.\\
	Si $u < v < 0$, alors $vu > 0$ et $v - u > 0$. On a donc \\
	$\frac1u - \frac1v > 0$, c'est-à-dire $\frac1u > \frac1v$ donc $f$ est décroissante.\\[1ex]
	Si $0 < u < v$, alors $vu > 0$ et $v - u > 0$. On a donc \\
	$\frac1u - \frac1v > 0$, c'est-à-dire $\frac1u > \frac1v$ donc $f$ est décroissante.
\end{proof}

\subsection{Représentation graphique de la fonction inverse}

\begin{definition}
	La courbe représentative de la fonction inverse dans un repère orthonormé s'appelle une \textbf{hyperbole}.
\end{definition}

\begin{center}
	\begin{tikzpicture}[>=latex]
	\draw [blue!25] (-6,-6) grid (6,6) ;
	\draw [thick,->] (-6,0) -- (6,0) ;
	\draw [thick,->] (0,-6) -- (0,6) ;
	\foreach \i in {1,...,5} { 
		\draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; 
		\draw (-\i,0.1) -- (-\i,-0.1) node [above] {$-\i$} ;
	}
	\foreach \i in {1,...,5}  { 
		\draw (0.1,\i) -- (-0.1,\i) node [left] 	{$\i$} ;
		\draw (0.1,-\i) -- (-0.1,-\i) node [right] 	{$\i$} ;
	}
	
	\draw [very thick,red] plot [domain=-6:-1/6,smooth] (\x,{1/\x}) ;
	\draw [very thick,red] plot [domain=1/6:6,smooth] (\x,{1/\x}) ; 
	
	\draw (1/3,3) node [fill,circle,blue,inner sep=1pt] {};
	\draw (1/3,3) node [below right,blue] {$M: (x,\frac1x)$} ;
	\draw (-1/3,-3) node [fill,circle,blue,inner sep=1pt] {};
	\draw (-1/3,-3) node [below left,blue] {$M': (-x,-\frac1x)$} ;
	\draw [thick,dashed,blue] (-1/3,-3) -- (1/3,3) ;
	\end{tikzpicture}
\end{center}

\begin{proposition}
	L'origine $O$ du repère est un centre de symétrie de la hyperbole.
\end{proposition}
\begin{proof}
  Soit $M$ un point de l' hyperoble. Ses coordonnées sont $M
  \pair*{x}{\frac1x}$. Le symétrique de $M$ par rapport à l'origine est le
  point $M'$ de coordonnées $M' \pair*{-x}{-\frac1x}$.
\end{proof}

\begin{info}[Parabole, hyperbole et ellipse]
	La parabole, l'hyperbole et l'ellipse ! Des notions rencontrées en Français, avec une signification différentes, des courbes de différentes fonctions, mais aussi les sections d'un cône de révolution avec un plan. Ces trois courbes ont été étudiées par Apollonius dans un traité sur les coniques.
\end{info}

\chapter{Fonctions polynomiales du second degré}

\section{Les fonctions $x \mapsto ax^2 + bx + c$ (avec $a≠0$)}

\subsection{Fonctions polynômes de degré 2}

\begin{definition}
  Une fonction polynomiale de degré 2 est une fonction $f$ définie sur $\R$
  par $f(x) = ax^2 + bx + c$, où $a,b,c$ désignent des nombres réels non
  nuls.
\end{definition}

\begin{exemple}
  \leavevmode
  \begin{itemize}
    \item $x \mapsto x^2$ la fonction carré ;
    \item $f(x) = -x^2 + 3x -1$ ;
    \item $x \mapsto (x - 5)^2$
  \end{itemize}
\end{exemple}

\subsection{Sens de variation et courbe représentatative}

\begin{note}
  En Première, on montrera que toute fonction polynomiale de degré 2
  s'écrivant $f(x) = ax^2 + bx + c$ peut se mettre sous la forme $f(x) = a(x
  + \alpha)^2 + \beta$. On appelle cette dernière forme, la forme canonique.
\end{note}

\begin{proposition}
  Soit $f(x) = ax^2 + bx +c$ une fonction définie pour $x \in \R$ et $a≠0$.
  Le tableau de variation est donné par
  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[deltacl=1]{$x$/1,$f$/3}{$-\infty$,$\alpha$,$+\infty$}
      \tkzTabVar{+/$+\infty$,-/$\beta$,+/$+\infty$}
    \end{tikzpicture}
  \end{center}
  si $a > 0$ et dans ce cas $\beta = f(\alpha)$ est le minimum de $f$ sur
  $\R$ et par
  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[deltacl=1]{$x$/1,$f$/3}{$-\infty$,$\alpha$,$+\infty$}
      \tkzTabVar{-/$-\infty$,+/$\beta$,-/$-\infty$}
    \end{tikzpicture}
  \end{center}
  si $a < 0$ et dans cas $\beta = f(\alpha)$ est le maximum de $f$ sur $\R$.
\end{proposition}
\begin{proof}
  Pour $a > 0$, on retrouve les variations de la fonction carré, comme
  démontré à la proposition \ref{fonctionref:prop:variations}.\\
  Si $x$ et $y$ sont deux nombres tels que $x < y \implies f(x) < f(y)$,
  alors, on a $f(x) - f(y) < 0$ et donc $a(f(x) - f(y))> 0 \iff af(x) >
  af(y)$.
\end{proof}

\begin{definition}
  Le point $S (\alpha,\beta)$ est appelé sommet de la parabole.
\end{definition}

\section{Signe d'un produit de facteurs du premier degré}

\subsection{Parabole et lecture graphique}

Soit $f$ une fonction définie, pour tout $x$ réel, par $f(x) = ax^2 + bx
+ c$, $a≠0$. D'après ce qui précède, cette parabole possède un sommet $S
(\alpha,\beta)$ et son «orientation» dépend du signe du coefficient $a$.
On peut alors distinguer quatre cas de figures distincts.

\begin{multicols}{2}
  \begin{center}
    \begin{tikzpicture}[scale=0.7]
      \draw[thin,dotted] (-2,-4) grid (5,5) ;
      \draw[->] (-2,0) -- (5,0) ;
      \draw[->] (0,-4) -- (0,5) ;

      \draw [red] plot [smooth,domain=-2:4] (\x,{(\x + 1)*(\x - 3)}) ;
    \end{tikzpicture}
  \end{center}
  \textbf{Cas $a > 0$ et $\beta < 0$} : la parabole coupe deux fois
  l'axe des abscisses et le tableau de signe est le suivant :
  \begin{center}
    \begin{tikzpicture}[scale=0.7]
      \tkzTabInit[deltacl=0.5]{$x$/1,$f$/1}{$-\infty$,$x_1$,$x_2$,$+\infty$}
      \tkzTabLine{,+,z,-,z,+,}
    \end{tikzpicture}
  \end{center}

  \begin{center}
    \begin{tikzpicture}[scale=0.7]
      \draw[thin,dotted] (-2,-4) grid (5,5) ;
      \draw[->] (-2,0) -- (5,0) ;
      \draw[->] (0,-4) -- (0,5) ;

      \draw [red] plot [smooth,domain=-2:4] (\x,{1/4*(\x + 1)*(\x - 3) +
      2}) ;
    \end{tikzpicture}
  \end{center}
  \textbf{Cas $a > 0$ et $\beta > 0$} : la parabole ne coupe pas l'axe
  des abscisses et son signe est constant :
  \begin{center}
    \begin{tikzpicture}[scale=0.7]
      \tkzTabInit[deltacl=0.5]{$x$/1,$f$/1}{$-\infty$,$+\infty$}
      \tkzTabLine{,+,}
    \end{tikzpicture}
  \end{center}


  \begin{center}
    \begin{tikzpicture}[scale=0.7]
      \draw[thin,dotted] (-2,-4) grid (5,5) ;
      \draw[->] (-2,0) -- (5,0) ;
      \draw[->] (0,-4) -- (0,5) ;

      \draw [red] plot [smooth,domain=-2:4] (\x,{-3/4*(\x + 1)*(\x - 3)}) ;
    \end{tikzpicture}
  \end{center}
  \textbf{Cas $a > 0$ et $\beta < 0$} : la parabole coupe deux fois
  l'axe des abscisses et le tableau de signe est le suivant :
  \begin{center}
    \begin{tikzpicture}[scale=0.7]
      \tkzTabInit[deltacl=0.5]{$x$/1,$f$/1}{$-\infty$,$x_1$,$x_2$,$+\infty$}
      \tkzTabLine{,-,z,+,z,-,}
    \end{tikzpicture}
  \end{center}

  \begin{center}
    \begin{tikzpicture}[scale=0.7]
      \draw[thin,dotted] (-2,-4) grid (5,5) ;
      \draw[->] (-2,0) -- (5,0) ;
      \draw[->] (0,-4) -- (0,5) ;

      \draw [red] plot [smooth,domain=-2:4] (\x,{-1/4*(\x + 1)*(\x - 3)
      - 2}) ;
    \end{tikzpicture}
  \end{center}
  \textbf{Cas $a > 0$ et $\beta < 0$} : la parabole ne coupe pas l'axe
  des abscisses et son signe est constant :
  \begin{center}
    \begin{tikzpicture}[scale=0.7]
      \tkzTabInit[deltacl=0.5]{$x$/1,$f$/1}{$-\infty$,$+\infty$}
      \tkzTabLine{,-,}
    \end{tikzpicture}
  \end{center}

\end{multicols}

\subsection{Tableau de signes}

Soit $f$ une fonction polynomiale de degré 2, c'est à dire telle que
$f(x) = ax^2 + bx + c$, $a ≠ 0$. On suppose qu'il existe $x_1$ et $x_2$
deux nombres réels tels que, pour tout $x$ réel, $f(x) = a(x - x_1)(x -
x_2)$.

\begin{savoirfaire}[Vérifier que deux expressions algébriques sont
  égales]
  Pour vérifier que deux expressions algébriques $A$ et $B$, on peut :
  \begin{itemize}
    \item factoriser ou développer $A$ pour trouver $B$ ;
    \item factoriser ou développer $B$ pour trouver $A$ ;
    \item factoriser ou développer $A$ et $B$ pour trouver une
      expression commune $C$.
  \end{itemize}
  \begin{exemple}
    Montrer que, pour tout $x$ réel, $x^2 - 4x + 3 = (x - 3)(x - 1)$.
  \end{exemple}
\end{savoirfaire}

\begin{proposition}
  \label{prop:polynomiale:signe}
  Soient $a$ et $b$ deux nombres réels.
  \begin{itemize}
    \item Si $a$ ou $b$ est nul, alors $ab = 0$.
    \item Si $a$ et $b$ sont de même signe, alors $ab > 0$.
    \item Si $a$ et $b$ sont de signe opposé, alors $ab < 0$.
  \end{itemize}
\end{proposition}
\begin{proof}
  Soient $a$ et $b$ deux nombres réels.
  \begin{itemize}
    \item Évident
    \item Si $a$ et $b$ sont tous deux positifs, alors $ab > 0$.
    \item Supposons $a > 0$ et $b > 0$. On a alors $0 = a(b + (-b)) = a×b
      + a×(-b)$. On en déduit que $-(a×b) = a×(-b)$ et donc que le
      produit d'un nombre négatif et d'un nombre positif est négatif.
    \item En appliquant à nouveau le résultat précédent, on prouve le
      dernier cas de figure : le produit de deux nombres négatifs est
      positif.
  \end{itemize}
\end{proof}

\begin{savoirfaire}[Construire le tableau de signe]
  Soit $f$ la fonction définie par $f(x) = (3x - 1)(-x + 2)$

  \begin{center}
    \begin{tikzpicture}[scale=0.7]
      \tkzTabInit[deltacl=1.5]{$x$/1,$3x - 1$/1,$-x + 2$/1,$f(x)$/1}
      {$-\infty$,$\frac13$,$2$,$+\infty$}
      \tkzTabLine{,-,z,+, ,+,}
      \tkzTabLine{,+, ,+,z,-,}
      \tkzTabLine{,-,z,+,z,-,}
    \end{tikzpicture}
  \end{center}
  On place les lignes correspondant aux facteurs. On place dans la
  première ligne les solutions des équations. On construit la denière
  ligne en appliquant la proposition~\ref{prop:polynomiale:signe} (règle
  des signes.)
\end{savoirfaire}
